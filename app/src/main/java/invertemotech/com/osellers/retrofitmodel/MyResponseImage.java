
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class MyResponseImage {

    @SerializedName("ifile")
    @Expose
    private String ifile;

    public String getIfile() {
        return ifile;
    }

    public void setIfile(String ifile) {
        this.ifile = ifile;
    }

}

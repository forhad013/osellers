
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Subcategory {

    @SerializedName("subcatid")
    @Expose
    private String subcatid;
    @SerializedName("subcatagoryname")
    @Expose
    private String subcatagoryname;
    @SerializedName("sub_bangla_name")
    @Expose
    private String subBanglaName;
    @SerializedName("brands")
    @Expose
    private ArrayList<Brand> brands = null;

    public String getSubcatid() {
        return subcatid;
    }

    public void setSubcatid(String subcatid) {
        this.subcatid = subcatid;
    }

    public String getSubcatagoryname() {
        return subcatagoryname;
    }

    public void setSubcatagoryname(String subcatagoryname) {
        this.subcatagoryname = subcatagoryname;
    }

    public String getSubBanglaName() {
        return subBanglaName;
    }

    public void setSubBanglaName(String subBanglaName) {
        this.subBanglaName = subBanglaName;
    }

    public ArrayList<Brand> getBrands() {
        return brands;
    }

    public void setBrands(ArrayList<Brand> brands) {
        this.brands = brands;
    }

}

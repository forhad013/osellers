
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Division {

    @SerializedName("div_id")
    @Expose
    private String divId;
    @SerializedName("div_name")
    @Expose
    private String divName;
    @SerializedName("div_name_bangla")
    @Expose
    private String divNameBangla;
    @SerializedName("districts")
    @Expose
    private ArrayList<District> districts = null;

    public String getDivId() {
        return divId;
    }

    public void setDivId(String divId) {
        this.divId = divId;
    }

    public String getDivName() {
        return divName;
    }

    public void setDivName(String divName) {
        this.divName = divName;
    }

    public String getDivNameBangla() {
         // divNameBangla= StringEscapeUtils.escapeJava(divNameBangla);


        return divNameBangla;


    }

    public void setDivNameBangla(String divNameBangla) {
        this.divNameBangla = divNameBangla;
    }

    public ArrayList<District> getDistricts() {
        return districts;
    }

    public void setDistricts(ArrayList<District> districts) {
        this.districts = districts;
    }

}

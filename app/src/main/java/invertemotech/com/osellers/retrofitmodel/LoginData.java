
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("osellers_user_id")
    @Expose
    private String osellersUserId;
    @SerializedName("Utype")
    @Expose
    private String utype;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("useremail")
    @Expose
    private String useremail;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("contactnum")
    @Expose
    private String contactnum;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("jdate")
    @Expose
    private String jdate;
    @SerializedName("jtime")
    @Expose
    private String jtime;
    @SerializedName("jdatetime")
    @Expose
    private String jdatetime;

    public String getOsellersUserId() {
        return osellersUserId;
    }

    public void setOsellersUserId(String osellersUserId) {
        this.osellersUserId = osellersUserId;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getContactnum() {
        return contactnum;
    }

    public void setContactnum(String contactnum) {
        this.contactnum = contactnum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJdate() {
        return jdate;
    }

    public void setJdate(String jdate) {
        this.jdate = jdate;
    }

    public String getJtime() {
        return jtime;
    }

    public void setJtime(String jtime) {
        this.jtime = jtime;
    }

    public String getJdatetime() {
        return jdatetime;
    }

    public void setJdatetime(String jdatetime) {
        this.jdatetime = jdatetime;
    }

}

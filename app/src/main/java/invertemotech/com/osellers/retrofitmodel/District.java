
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class District {

    @SerializedName("dis_id")
    @Expose
    private String disId;
    @SerializedName("dis_name")
    @Expose
    private String disName;
    @SerializedName("dis_name_bangla")
    @Expose
    private String disNameBangla;
    @SerializedName("areas")
    @Expose
    private ArrayList<Area> areas = null;

    public String getDisId() {
        return disId;
    }

    public void setDisId(String disId) {
        this.disId = disId;
    }

    public String getDisName() {
        return disName;
    }

    public void setDisName(String disName) {
        this.disName = disName;
    }

    public String getDisNameBangla() {
        return disNameBangla;
    }

    public void setDisNameBangla(String disNameBangla) {
        this.disNameBangla = disNameBangla;
    }

    public ArrayList<Area> getAreas() {
        return areas;
    }

    public void setAreas(ArrayList<Area> areas) {
        this.areas = areas;
    }

}

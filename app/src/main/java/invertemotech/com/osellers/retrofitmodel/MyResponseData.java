
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
@Parcel
public class MyResponseData {

    @SerializedName("advertise_id")
    @Expose
    private String advertiseId;
    @SerializedName("productname")
    @Expose
    private String productname;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("expirydate")
    @Expose
    private String expirydate;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("advertise_date")
    @Expose
    private String advertiseDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("upload_id")
    @Expose
    private String uploadId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("ad_details_id")
    @Expose
    private String adDetailsId;
    @SerializedName("ad_id")
    @Expose
    private String adId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contactnum")
    @Expose
    private String contactnum;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("subcatagoryid")
    @Expose
    private String subcatagoryid;
    @SerializedName("subcatagoryname")
    @Expose
    private String subcatagoryname;
    @SerializedName("sub_bangla_name")
    @Expose
    private String subBanglaName;
    @SerializedName("area_name")
    @Expose
    private String areaName;
    @SerializedName("area_code")
    @Expose
    private String areaCode;
    @SerializedName("dis_id")
    @Expose
    private String disId;
    @SerializedName("area_name_bangla")
    @Expose
    private String areaNameBangla;
    @SerializedName("district_id")
    @Expose
    private String districtId;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("discode")
    @Expose
    private String discode;
    @SerializedName("division_id")
    @Expose
    private String divisionId;
    @SerializedName("district_bangla")
    @Expose
    private String districtBangla;
    @SerializedName("division_name")
    @Expose
    private String divisionName;
    @SerializedName("Division_Code")
    @Expose
    private String divisionCode;
    @SerializedName("Desciption")
    @Expose
    private String desciption;
    @SerializedName("division_name_bangla")
    @Expose
    private String divisionNameBangla;
    @SerializedName("images")
    @Expose
    private ArrayList<MyResponseImage> images = null;

    public String getAdvertiseId() {
        return advertiseId;
    }

    public void setAdvertiseId(String advertiseId) {
        this.advertiseId = advertiseId;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdvertiseDate() {
        return advertiseDate;
    }

    public void setAdvertiseDate(String advertiseDate) {
        this.advertiseDate = advertiseDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdDetailsId() {
        return adDetailsId;
    }

    public void setAdDetailsId(String adDetailsId) {
        this.adDetailsId = adDetailsId;
    }

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactnum() {
        return contactnum;
    }

    public void setContactnum(String contactnum) {
        this.contactnum = contactnum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSubcatagoryid() {
        return subcatagoryid;
    }

    public void setSubcatagoryid(String subcatagoryid) {
        this.subcatagoryid = subcatagoryid;
    }

    public String getSubcatagoryname() {
        return subcatagoryname;
    }

    public void setSubcatagoryname(String subcatagoryname) {
        this.subcatagoryname = subcatagoryname;
    }

    public String getSubBanglaName() {
        return subBanglaName;
    }

    public void setSubBanglaName(String subBanglaName) {
        this.subBanglaName = subBanglaName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getDisId() {
        return disId;
    }

    public void setDisId(String disId) {
        this.disId = disId;
    }

    public String getAreaNameBangla() {
        return areaNameBangla;
    }

    public void setAreaNameBangla(String areaNameBangla) {
        this.areaNameBangla = areaNameBangla;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDiscode() {
        return discode;
    }

    public void setDiscode(String discode) {
        this.discode = discode;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDistrictBangla() {
        return districtBangla;
    }

    public void setDistrictBangla(String districtBangla) {
        this.districtBangla = districtBangla;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public String getDivisionNameBangla() {
        return divisionNameBangla;
    }

    public void setDivisionNameBangla(String divisionNameBangla) {
        this.divisionNameBangla = divisionNameBangla;
    }

    public ArrayList<MyResponseImage> getImages() {
        return images;
    }

    public void setImages(ArrayList<MyResponseImage> images) {
        this.images = images;
    }

}

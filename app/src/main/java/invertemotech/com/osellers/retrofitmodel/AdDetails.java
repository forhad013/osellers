
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;
@Parcel
public class AdDetails {

    @SerializedName("advertise_id")
    @Expose
    public String advertiseId;
    @SerializedName("productname")
    @Expose
    public String productname;
    @SerializedName("cat_id")
    @Expose
    public String catId;
    @SerializedName("brand_id")
    @Expose
    public String brandId;
    @SerializedName("expirydate")
    @Expose
    public String expirydate;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("advertise_date")
    @Expose
    public String advertiseDate;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("upload_id")
    @Expose
    public String uploadId;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("contactnum")
    @Expose
    public String contactnum;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("areaid")
    @Expose
    public String areaid;
    @SerializedName("area_name")
    @Expose
    public String areaName;
    @SerializedName("area_name_bangla")
    @Expose
    public String areaNameBangla;
    @SerializedName("districtid")
    @Expose
    public String districtid;
    @SerializedName("district")
    @Expose
    public String district;
    @SerializedName("district_bangla")
    @Expose
    public String districtBangla;
    @SerializedName("divisionid")
    @Expose
    public String divisionid;
    @SerializedName("division_name")
    @Expose
    public String divisionName;
    @SerializedName("division_name_bangla")
    @Expose
    public String divisionNameBangla;
    @SerializedName("subcatagoryid")
    @Expose
    public String subcatagoryid;
    @SerializedName("subcatagoryname")
    @Expose
    public String subcatagoryname;
    @SerializedName("subcatbangla")
    @Expose
    public String subcatbangla;
    @SerializedName("images")
    @Expose
    public ArrayList<Image> images = null;

    public String getAdvertiseId() {
        return advertiseId;
    }

    public void setAdvertiseId(String advertiseId) {
        this.advertiseId = advertiseId;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdvertiseDate() {
        return advertiseDate;
    }

    public void setAdvertiseDate(String advertiseDate) {
        this.advertiseDate = advertiseDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactnum() {
        return contactnum;
    }

    public void setContactnum(String contactnum) {
        this.contactnum = contactnum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAreaid() {
        return areaid;
    }

    public void setAreaid(String areaid) {
        this.areaid = areaid;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaNameBangla() {
        return areaNameBangla;
    }

    public void setAreaNameBangla(String areaNameBangla) {
        this.areaNameBangla = areaNameBangla;
    }

    public String getDistrictid() {
        return districtid;
    }

    public void setDistrictid(String districtid) {
        this.districtid = districtid;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrictBangla() {
        return districtBangla;
    }

    public void setDistrictBangla(String districtBangla) {
        this.districtBangla = districtBangla;
    }

    public String getDivisionid() {
        return divisionid;
    }

    public void setDivisionid(String divisionid) {
        this.divisionid = divisionid;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getDivisionNameBangla() {
        return divisionNameBangla;
    }

    public void setDivisionNameBangla(String divisionNameBangla) {
        this.divisionNameBangla = divisionNameBangla;
    }

    public String getSubcatagoryid() {
        return subcatagoryid;
    }

    public void setSubcatagoryid(String subcatagoryid) {
        this.subcatagoryid = subcatagoryid;
    }

    public String getSubcatagoryname() {
        return subcatagoryname;
    }

    public void setSubcatagoryname(String subcatagoryname) {
        this.subcatagoryname = subcatagoryname;
    }

    public String getSubcatbangla() {
        return subcatbangla;
    }

    public void setSubcatbangla(String subcatbangla) {
        this.subcatbangla = subcatbangla;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

}

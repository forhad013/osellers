
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Category {

    @SerializedName("catid")
    @Expose
    private String catid;
    @SerializedName("catname")
    @Expose
    private String catname;
    @SerializedName("catnamebangla")
    @Expose
    private String catnamebangla;
    @SerializedName("subcategories")
    @Expose
    private ArrayList<Subcategory> subcategories = null;

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getCatnamebangla() {
        return catnamebangla;
    }

    public void setCatnamebangla(String catnamebangla) {
        this.catnamebangla = catnamebangla;
    }

    public ArrayList<Subcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(ArrayList<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

}

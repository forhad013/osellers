
package invertemotech.com.osellers.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Brand {

    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("brandname")
    @Expose
    private String brandname;
    @SerializedName("brand_bangla_name")
    @Expose
    private String brandBanglaName;

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getBrandBanglaName() {
        return brandBanglaName;
    }

    public void setBrandBanglaName(String brandBanglaName) {
        this.brandBanglaName = brandBanglaName;
    }

}

package invertemotech.com.osellers.retrofitmodel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mac on 8/11/16.
 */
public interface ApiInterafce {


    public static final String SERVER_DOMAIN = "http://goodtill.rskyinvestment.com/api/v1.1/";
    String refreshUrl ="v1/refresh/";


    //registration
//    @POST("register")
//    Call<RegResponse> regitser(@Query("appid") String appid, @Query("name") String name,
//                               @Query("gender") String gender, @Query("age") String age,
//                               @Query("province") String province, @Query("email") String email,
//                               @Query("password") String password, @Query("gcm") String gcm);
//
//
//    @POST("login")
//    Call<LoginResponse> login(@Query("appid") String appid, @Query("email") String email,
//                              @Query("password") String password, @Query("gcm") String gcm);


        //topicsList
    @GET("locations")
    Call<LocationResponse> getLocationList();

    @GET("categories")
    Call<CategoryResponse> getCatgoryList();



    @GET("advertise/{subcategoryid}/{brandsid}/{areaid}/{districtid}/{divisionid}/{startprice}/{endprice}/{searchkey}")
    Call<AdsResponse> getAds(@Path("subcategoryid") int subcategoryid,
                             @Path("brandsid") String brandsid,
                             @Path("areaid") int areaid,

                             @Path("districtid") int districtid,
                             @Path("divisionid") int divisionid,
                             @Path("startprice") String startprice,
                             @Path("endprice") String endprice,
                             @Path("searchkey") String searchkey,
                             @Query("page") String page);

//
//
    @GET("useradvertises/{email}")
    Call<MyPostResponse> getMyAds(@Path("email") String email);


    @POST("register")
    Call<UpdateResponse> regitser(@Query("name") String name, @Query("email") String email,
                               @Query("contactno") String contactno,
                               @Query("confirmpass") String confirmpass,
                               @Query("password") String password);


    @POST("login")
    Call<LoginResponse> login( @Query("email") String email,
                              @Query("password") String password);
    @POST("profileupdate/{id}")
    Call<UpdateResponse> editProfile(@Path("id") int id,
                              @Query("contactnum") String contact);


    @POST("passwordchange/{id}")
    Call<UpdateResponse> changePassword( @Path("id") int id,
                              @Query("oldpassword") String oldpassword,
                              @Query("newpassword") String newpassword);


    @Multipart
    @POST("postadvertise")
    Call<UpdateResponse> postAdd(
                              @Query("productname") String productname,
                              @Query("cat_id") String cat_id,
                              @Query("brand_id") String brand_id,
                              @Query("description") String description,
                              @Query("price") Float price,
                              @Query("name") String name,
                              @Query("area_id") String area_id,
                              @Query("email") String email,
                              @Query("contactnum") String contactnum,
                              @Query("address") String address,

                              @Part MultipartBody.Part[] postImage);




}

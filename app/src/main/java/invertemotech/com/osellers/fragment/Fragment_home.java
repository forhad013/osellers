package invertemotech.com.osellers.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.parceler.Parcels;

import java.util.ArrayList;

import invertemotech.com.osellers.R;
import invertemotech.com.osellers.activity.Activity_post_details;
import invertemotech.com.osellers.activity.CategoryActivity;
import invertemotech.com.osellers.activity.LocationActivity;
import invertemotech.com.osellers.activity.LoginActivity;
import invertemotech.com.osellers.activity.NewPostActivity;
import invertemotech.com.osellers.adapter.AdsAdapter;
import invertemotech.com.osellers.adapter.EndlessScrollListener;
import invertemotech.com.osellers.retrofitmodel.AdDetails;
import invertemotech.com.osellers.retrofitmodel.AdsResponse;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_home extends Fragment {

    int pageNumber =1;

    int RESPONSE_CODE_LOCATION = 123;
    int RESPONSE_CODE_CATEGORY = 321;
    LinearLayout locationBtn,categoryBtn;
    ImageButton post;
    SharePref sharePref;
    EditText searchBox;
    String selectedDivision="0",selectedDistrict="0",selectedArea="0";

    String searchkey;

    String districtName="";
    String divisionName="";
    String areaName="";
    TextView locationName,categoryName;
    String brandId = "0",subCategoryId="0",price1="",price2="",subCateogryName="";


    int divisionPosition=200;
    int districtPosition=200;
    int areaPosition=200;

    int  brandPosition = 200;
    int  subcateogryPosition =200;
    int  categoryPosition = 200;

    String selectedCategory;

    LinearLayout emptyLayout;

    
    ArrayList<AdDetails> adDetailsArrayList;

    ConnectionDetector cd;
    boolean isInternetON;
    Context context;
    ArrayList<Boolean> brandList;

    boolean hasSetLocation = false;
    boolean hasSetCategory = false;
    AdsAdapter adsAdapter;
    PullToRefreshListView listView;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get the view from fragmenttab1.xml
        View view = inflater.inflate(R.layout.fragment_home, container,
                false);


        context = getActivity();
        adDetailsArrayList = new ArrayList<>();


       listView = (PullToRefreshListView) view.findViewById(R.id.list);
        cd = new ConnectionDetector(context);
        isInternetON = cd.isConnectingToInternet();

        sharePref = new SharePref(context);

        locationBtn = (LinearLayout)  view.findViewById(R.id.locationBtn);
        categoryBtn = (LinearLayout) view.findViewById(R.id.categoryBtn);
        searchBox = (EditText) view.findViewById(R.id.searchBox);
        locationName = (TextView) view.findViewById(R.id.location);
        categoryName = (TextView) view.findViewById(R.id.category);

        emptyLayout = (LinearLayout) view.findViewById(R.id.empty);

        adsAdapter = new AdsAdapter(context,adDetailsArrayList);

        listView.setAdapter(adsAdapter);


        searchBox.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
                    String ex = searchBox.getText().toString();
                    Log.e("ex",ex);

                }
                return false;
            }
        });

        hasSetLocation = false;

        post = (ImageButton) view.findViewById(R.id.post);


        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sharePref.getshareprefdataBoolean(SharePref.LOGEDIN)){

                    Intent intent=new Intent(getActivity(),NewPostActivity.class);
                    startActivity(intent);

                }else{

                    Intent i = new Intent(getActivity(),LoginActivity.class);
                    startActivity(i);

                }

              // getActivity().finish();
            }
        });



        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetON) {

                    Intent intent = new Intent(getActivity(), LocationActivity.class);
                    intent.putExtra("divisionPosition", divisionPosition);
                    intent.putExtra("districtPosition", districtPosition);
                    intent.putExtra("areaPosition", areaPosition);
                    intent.putExtra("setLocation", hasSetLocation);

                    intent.putExtra("division", divisionPosition);
                    intent.putExtra("district", districtPosition);
                    intent.putExtra("area", areaPosition);

                    startActivityForResult(intent, RESPONSE_CODE_LOCATION);
                }else {
                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();

                }
//                getActivity().overridePendingTransition(R.anim.nothing, R.anim.slideout_to_bottom);
//                getActivity().overridePendingTransition();

            }
        });
        categoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetON) {

                    Intent intent = new Intent(getActivity(), CategoryActivity.class);
                    intent.putExtra("brandPosition", brandList);
                    intent.putExtra("subcateogryPosition", subcateogryPosition);
                    intent.putExtra("categoryPosition", categoryPosition);

                    intent.putExtra("setCategory", hasSetCategory);
                    intent.putExtra("price1", price1);
                    intent.putExtra("price2", price2);
                    startActivityForResult(intent, RESPONSE_CODE_CATEGORY);
//                getActivity().overridePendingTransition(R.anim.nothing, R.anim.slideout_to_bottom);
//                getActivity().overridePendingTransition();
                }else{
                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();

                }

            }
        });

        if(isInternetON){
            getAds();
        }


        searchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if(isInternetON){

                        adDetailsArrayList.clear();
                        adsAdapter.notifyDataSetChanged();

                        pageNumber =1;
                        getAds();
                    }
                    return true;
                }
                return false;
            }
        });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Parcelable listParcelable = Parcels.wrap(adDetailsArrayList);

                Intent intent = new Intent(getActivity(), Activity_post_details.class);
                intent.putExtra("ad",listParcelable);
                intent.putExtra("position",position-1);
                startActivity(intent);
            }
        });


        listView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView


                if(adDetailsArrayList.size()>pageNumber)
                pageNumber++;

                Log.e("pg",pageNumber+"");



                getAds();

            }
        });

        listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                // Do work to refresh the list here.
                pageNumber = 1;

                adDetailsArrayList.clear();

                getAds();
            }
        });


        return view;
    }

    @Override
        public void onResume() {
        adsAdapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==RESPONSE_CODE_LOCATION)
        {
            try {
                //String message=data.getStringExtra("location");
                divisionName = data.getStringExtra("DivisionName");
                districtName = data.getStringExtra("DistrictName");
                areaName = data.getStringExtra("AreaName");
                selectedDistrict = data.getStringExtra("DistrictId");
                selectedDivision = data.getStringExtra("DivisionId");
                selectedArea = data.getStringExtra("AreaId");
                hasSetLocation = data.getBooleanExtra("setLocation",false);

                data.putExtra("divisionPosition",divisionPosition);
                data.putExtra("districtPosition",districtPosition);
                data.putExtra("areaPosition",areaPosition);

                divisionPosition = data.getIntExtra("division",200);
                districtPosition = data.getIntExtra("district",200);
                areaPosition = data.getIntExtra("area",200);


                if (!areaName.equals("")) {
                    locationName.setText(areaName);
                } else if (!districtName.equals("")) {
                    locationName.setText(districtName);
                } else if (!divisionName.equals("")) {
                    locationName.setText(divisionName);
                }

                adDetailsArrayList.clear();
                pageNumber =1;
                getAds();

            }catch (Exception e){
                e.printStackTrace();
            }

    }else if(requestCode==RESPONSE_CODE_CATEGORY)
        {
            try {
                //String message=data.getStringExtra("location");

                brandId = data.getStringExtra("brandId");
                subCategoryId = data.getStringExtra("subcategoryId");
                subCateogryName = data.getStringExtra("subcategoryName");
                price1 = data.getStringExtra("price1");
                price2 = data.getStringExtra("price2");
                subcateogryPosition = data.getIntExtra("subcateogryPosition",200);
                categoryPosition = data.getIntExtra("categoryPosition",200);

                selectedCategory = data.getStringExtra("selectedCateogry");


                brandList = (ArrayList<Boolean>) data.getSerializableExtra("brandPosition");

                hasSetCategory = data.getBooleanExtra("setCategory",false);

                if (!subCateogryName.equals("")) {
                    categoryName.setText(subCateogryName);
                } else  {
                    categoryName.setText("All Category");
                }
                if(selectedCategory.equals("0")){
                    categoryName.setText("All Category");
                }

                adDetailsArrayList.clear();
                pageNumber =1;
                getAds();
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }



    public void getAds(){

         
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        searchkey = searchBox.getText().toString().trim();
        if(searchkey.equals("")){
            searchkey="0";
        }

        if(price1.equals("")){
            price1 = "00";
        }
        if(price2.equals("")){
            price2 = "00";
        }

        Log.e("pg",pageNumber+"");
        Log.e("searchkey",searchkey+"");

        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Log.e("brandId",brandId);
        Log.e("selectedDivision",selectedDivision);
        Log.e("selectedDistrict",selectedDistrict);
        Log.e("selectedArea",selectedArea);
        Log.e("price1",price1);
        Log.e("price2",price2);
        Log.e("subCategoryId",subCategoryId);


        Call<AdsResponse> call = apiService.getAds(Integer.parseInt(subCategoryId),
                brandId ,

                Integer.parseInt(selectedArea),
                Integer.parseInt(selectedDistrict),
                Integer.parseInt(selectedDivision),
                price1,price2,searchkey,pageNumber+"");


             call.enqueue(new Callback<AdsResponse>() {
            @Override
            public void onResponse(Call<AdsResponse>call, Response<AdsResponse> response) {



                try {


                    ArrayList<AdDetails> ads = response.body().getAdsData().getData();


                    boolean error = response.body().getError();

                    String msg = response.body().getMessage();

                    Log.e("error", error + "");
                    Log.e("msg", msg + "");


                    if (!error) {
                        emptyLayout.setVisibility(View.GONE);

                        for (int i = 0; i < ads.size(); i++) {
                            adDetailsArrayList.add(ads.get(i));
                        }

                        adsAdapter.notifyDataSetChanged();
                        listView.onRefreshComplete();

//                    Log.e("a",response.body().getAdsData().getNextPageUrl());

                    } else {

                    }


                }catch (Exception e){

                    e.printStackTrace();

                    if(adDetailsArrayList.size()==0){
                    emptyLayout.setVisibility(View.VISIBLE);
                    }

                    Toast.makeText(getActivity(),"No More Ad",Toast.LENGTH_LONG).show();
                }
 


            }

            @Override
            public void onFailure(Call<AdsResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());

             
            }
        });


    }
}
package invertemotech.com.osellers.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.activity.HomeActivity;
import invertemotech.com.osellers.activity.MyPostsActivity;
import invertemotech.com.osellers.activity.PasswordChange;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.retrofitmodel.UpdateResponse;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFragment extends Fragment {

    TextView TopBarText, firstNameTextT, lastNameTextT, emailTextT, phoneTextT;
    EditText firstNameText, emailText, phoneText;
    String firstNameTextString, lastNameTextString, emailTextString, phoneTextString;
    ScrollView mainLayout;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    ImageButton nearestTxt, edittext;
    Button update,changePassword,logoutBtn,myPost;

    String firstNameString, lastNameString, emailString, addressString, phoneString, userIdString, passwordString1, passwordString2;

    LinearLayout shopList, inbox;

    ImageButton backBtn;


    public boolean editable = false;

    boolean isInternetOn = false;

    SharePref sharePref;

    JSONObject json;
    int success;
    String message;
    ImageButton cancel;

    Context context;

    ConnectionDetector cd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mainLayout = (ScrollView) view.findViewById(R.id.main);
        sharePref = new SharePref(getActivity());


        context = getActivity();

        firstNameText = (EditText) view.findViewById(R.id.profileFirstName);

        emailText = (EditText) view.findViewById(R.id.profileEmail);
        phoneText = (EditText) view.findViewById(R.id.profileContact);

        firstNameTextT = (TextView) view.findViewById(R.id.profileFirstNam);

        emailTextT = (TextView) view.findViewById(R.id.profileEmailT);
        phoneTextT = (TextView) view.findViewById(R.id.profileContactT);
        cancel = (ImageButton) view.findViewById(R.id.cancel);
        edittext = (ImageButton) view.findViewById(R.id.edit);
        update = (Button) view.findViewById(R.id.update);
        logoutBtn = (Button) view.findViewById(R.id.logout);
        myPost = (Button) view.findViewById(R.id.myPost);
        changePassword = (Button) view.findViewById(R.id.changePassword);


        disableAll();


        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();

        firstNameString = sharePref.getshareprefdatastring(SharePref.USERNAME);

        emailString = sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        phoneString = sharePref.getshareprefdatastring(SharePref.USERPHONE);
        userIdString = sharePref.getshareprefdatastring(SharePref.USERID);

        firstNameText.setText(firstNameString);

        emailText.setText(emailString);

        if (!phoneString.isEmpty()) {
            phoneText.setText(phoneString);
        } else {
            phoneText.setText("Please update your phone number");
        }

        update.setVisibility(View.GONE);

        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
        edittext.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (!editable) {
                    enableAll();
                    cancel.setVisibility(View.VISIBLE);
                    editable = true;
                    update.setVisibility(View.VISIBLE);

                } else {
                    disableAll();
                    editable = false;
                }
            }
        });




        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharePref.setshareprefdataBoolean(SharePref.LOGEDIN,false);

                ((HomeActivity)getActivity()).LoadHome();

            }
        });
        cancel.setVisibility(View.INVISIBLE);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstNameTextString = firstNameText.getText().toString();

                emailTextString = emailText.getText().toString();
                phoneTextString = phoneText.getText().toString();
                emailTextString = emailText.getText().toString();
                cd = new ConnectionDetector(getActivity());

                isInternetOn = cd.isConnectingToInternet();

                if (isInternetOn) {

                    if (!phoneTextString.equals("")) {

                        editProfile();

                    } else {
                        doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                        doneDialog.setTitleText("ERROR");
                        doneDialog.setContentText("Empty Contact Number");
                        doneDialog.show();
                        doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                doneDialog.dismiss();
                            }
                        });

                    }


                } else {
                    Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


                }
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableAll();
                update.setVisibility(View.INVISIBLE);
                cancel.setVisibility(View.INVISIBLE);
            }
        });


        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(),PasswordChange.class);
                startActivity(i);
            }
        });

        myPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(),MyPostsActivity.class);
                startActivity(i);
            }
        });

        myPost.setVisibility(View.GONE);
        return view;
    }


    @Override
    public void onResume() {

        super.onResume();
    }


    public void setData() {


        firstNameString = sharePref.getshareprefdatastring(SharePref.USERNAME);

        emailString = sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        phoneString = sharePref.getshareprefdatastring(SharePref.USERPHONE);
        userIdString = sharePref.getshareprefdatastring(SharePref.USERID);

        firstNameText.setText(firstNameString);

        emailText.setText(emailString);

        if (!phoneString.isEmpty()) {
            phoneText.setText(phoneString);
        } else {
            phoneText.setText("Please update your phone number");
        }

    }


    public void editProfile(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);


        Call<UpdateResponse> call = apiService.editProfile(Integer.parseInt(userIdString),phoneTextString);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();


                Log.e("error",error+"");
                Log.e("msg",msg+"");

                progressSweetAlertDialog.dismiss();
                if(!error){


                    sharePref.setshareprefdatastring(SharePref.USERPHONE,phoneTextString);
                    disableAll();
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText(msg);
                    doneDialog.setConfirmText("Ok");
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();


                        }
                    });
                    doneDialog.show();




                }else {
                    progressSweetAlertDialog.dismiss();
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText("ERROR");
                    doneDialog.setContentText(msg);
                    doneDialog.show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }


    public void disableAll() {
        firstNameText.setEnabled(false);
        emailText.setEnabled(false);
        phoneText.setEnabled(false);

        update.setVisibility(View.INVISIBLE);
        cancel.setVisibility(View.INVISIBLE);

    }

    public void enableAll() {
        firstNameText.setEnabled(false);
        emailText.setEnabled(false);
        phoneText.setEnabled(true);



    }

//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }
}

package invertemotech.com.osellers.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import invertemotech.com.osellers.R;


public class AddImageAdapter extends BaseAdapter {


	onItemClickListener callback;



	ArrayList<String> images;

	private Context context;
	SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
	//SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy | hh:mm a");

	SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy");


	public AddImageAdapter(Context context,
						   ArrayList<String> images) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.images = images;






	}

	public interface onItemClickListener {
		public void onItemClicked(int position);

	}

	public void setCustomObjectListener(onItemClickListener listener) {
		this.callback = listener;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return images.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


		//	if (view == null) {

		view = LayoutInflater.from(context).inflate(
				R.layout.add_image_item, parent, false);
		//	}


		ImageView image = (ImageView) view.findViewById(R.id.image);
		Button remove = (Button) view.findViewById(R.id.remove);

		//Uri myUri =  images.get(position) ;
		//	image.setImageBitmap(images.get(position));
		//Log.e("uriM",myUri+"");
		//image.setImageURI(myUri);

		remove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(callback!=null){
					callback.onItemClicked(position);
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});

		 String uri = images.get(position)+"";

		setCustomObjectListener(callback);
		// String path = Environment.getExternalStorageDirectory()+ uri+"";

		File imgFile = new File(images.get(position).toString());
		if (imgFile.exists()) {
			Bitmap myBitmap = BitmapFactory.decodeFile(images.get(position).toString() + "");

			image.setImageBitmap(myBitmap);

		//	image.setImageURI();

		}

			return view;
		}


}
package invertemotech.com.osellers.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import invertemotech.com.osellers.R;
import invertemotech.com.osellers.retrofitmodel.MyResponseImage;
import invertemotech.com.osellers.util.SharePref;


public class MyPostDetailsImageAdapter extends RecyclerView.Adapter<MyPostDetailsImageAdapter.MyViewHolder> {


	ArrayList<MyResponseImage> horizontalList ;
	Context context;
  SharePref sharePref;
	public MyPostDetailsImageAdapter(ArrayList<MyResponseImage> horizontalList, Context context) {
		this.horizontalList = horizontalList;
		this.context = context;

		sharePref = new SharePref(context);
	}


	public class MyViewHolder extends RecyclerView.ViewHolder {

		ImageView imageView;
		RelativeLayout main;
		ProgressBar progressBar;
		TextView loadText;
		public MyViewHolder(View view) {
			super(view);
			imageView=(ImageView) view.findViewById(R.id.image);
			main=(RelativeLayout) view.findViewById(R.id.main);
			progressBar=(ProgressBar) view.findViewById(R.id.progress);
			loadText=(TextView) view.findViewById(R.id.load);


		}
	}



	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_image_item, parent, false);


		return new MyViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(final MyViewHolder holder, final int position) {



//		DisplayMetrics displayMetrics = new DisplayMetrics();
//		((Activity) context).getWindowManager()
//				.getDefaultDisplay()
//				.getMetrics(displayMetrics);
//		int height = displayMetrics.heightPixels;
//		int width = displayMetrics.widthPixels;

		//main.setMinimumWidth(width-100);
//		holder.imageView.setMinimumWidth(width-20);
//
//		holder.imageView.getLayoutParams().width = width-40;
//		holder.imageView.getLayoutParams().height = width-40;
//		holder.imageView.requestLayout();


		final String url ="https://osellers.com/admin/"+horizontalList.get(position).getIfile();





		if(sharePref.getshareprefdataBoolean(SharePref.DONT_SHOW_IMAGE)){
		//	imageView.setVisibility(View.GONE);
			holder.loadText.setVisibility(View.VISIBLE);
		}else{

				Picasso.with(context).load(url)
						.into(holder.imageView);
		}


		holder.imageView.setOnClickListener(new View.OnClickListener() {
			@Override

			public void onClick(View v) {

				if(sharePref.getshareprefdataBoolean(SharePref.DONT_SHOW_IMAGE)){

					holder.imageView.setImageBitmap(null);
					holder.progressBar.setVisibility(View.VISIBLE);
					holder.loadText.setVisibility(View.GONE);


					Picasso.with(context)
							.load(url)
							.into(	holder.imageView, new Callback() {
								@Override
								public void onSuccess() {

									holder.progressBar.setVisibility(View.GONE)
									;
								}

								@Override
								public void onError() {

								}
							});
				}


			}

		});

	}


	@Override
	public int getItemCount()
	{
		return horizontalList.size();
	}
}
package invertemotech.com.osellers.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import invertemotech.com.osellers.R;
import invertemotech.com.osellers.retrofitmodel.MyResponseData;
import invertemotech.com.osellers.util.SharePref;


public class MyAdsAdapter extends BaseAdapter {


	Date currentTime;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	SharePref sharePref;

	ArrayList<MyResponseData> ads;
	Date date;
	private Context context;

	public MyAdsAdapter(Context context,
                        ArrayList<MyResponseData> ads) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.ads = ads;


		sharePref = new SharePref(context);


		Calendar now = Calendar.getInstance();

		currentTime = now.getTime();

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ads.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.ad_item, parent, false);
	//	}


		ImageView imageView = (ImageView) view.findViewById(R.id.image);

		TextView nameTxt = (TextView) view.findViewById(R.id.title);

		TextView info = (TextView) view.findViewById(R.id.info1);



		TextView time = (TextView) view.findViewById(R.id.time);


		TextView price = (TextView) view.findViewById(R.id.price);

		Log.e("a",ads.get(position).getDivisionName() +", "+ ads.get(position).getSubcatagoryname());

		if(sharePref.getshareprefdataBoolean(SharePref.DONT_SHOW_IMAGE)){
			imageView.setVisibility(View.GONE);
		}else{
			imageView.setVisibility(View.VISIBLE);

			try {
				String url = "https://osellers.com/admin/" + ads.get(position).getImages().get(0).getIfile();

				Picasso.with(context).load(url)
						.into(imageView);
			}catch (Exception e){

			}
		}

		Log.e("m",ads.get(position).getDivisionName());

		nameTxt.setText(ads.get(position).getProductname());
		price.setText("Price : "+ads.get(position).getPrice() +" Taka");
		try {
			info.setText(ads.get(position).getDivisionName() +", "+ ads.get(position).getSubcatagoryname());
		}catch (Exception e){

		}
		time.setText(getTime(ads.get(position).getAdvertiseDate()));





		return view;
	}



	public String getTime(String postDate){
		Date date = null;
		String time="";

	//	Log.e("p",postDate);
		try {
			date = sdf.parse(postDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}




		Calendar c = Calendar.getInstance();

		c.setTime(date);
		c.setTimeZone(TimeZone.getDefault());

	//	Log.e("Date",date+"");

		String s=  getCurrentTimezoneOffset();
		s= s.replace("G","");
		s= s.replace("M","");
		s= s.replace("T","");


		boolean hasPlusSign = s.contains("+");

		// Log.e("asd",hasPlusSign+"");
		if(hasPlusSign){
			s= s.replace("+","");
			String[] parts = s.split(":");
			c.add(Calendar.HOUR,Integer.parseInt(parts[0]));
			c.add(Calendar.MINUTE,Integer.parseInt(parts[1]));
		}else{
			s=   s.replace("-","");
			String[] parts = s.split(":");
			int k = - Integer.parseInt(parts[0]);
			int j = - Integer.parseInt(parts[1]);

			c.add(Calendar.HOUR,k);
			c.add(Calendar.MINUTE,j);
		}

		date = c.getTime();

		long diff = currentTime.getTime() - date.getTime();


		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		time = "";
		if (diffDays != 0) {

			if(diffDays==1){

				time = diffDays + " day ago ";
			}else {
				time = diffDays + " days ago";
			}
		}
		else if (diffHours != 0) {
			if(diffMinutes>40){
				diffHours++;
			}
			if(diffHours==1){
				time =  diffHours + " hour ago";
			}else {
				time =  diffHours + " hours ago";
			}

		}
		else if (diffMinutes != 0) {
			if(diffMinutes==1){
				time =  diffMinutes + " minute ago";
			}else {
				time =  diffMinutes + " minutes ago";
			}

		}else if (diffMinutes == 0) {

				time =  "Just Now";

		}





		return time;
	}

	public String getCurrentTimezoneOffset() {

		TimeZone tz = TimeZone.getDefault();
		Calendar cal = GregorianCalendar.getInstance(tz);
		int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

		String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
		offset = "GMT"+(offsetInMillis >= 0 ? "+" : "-") + offset;

		return offset;
	}



}
package invertemotech.com.osellers.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.adapter.MyPostDetailsImageAdapter;
import invertemotech.com.osellers.retrofitmodel.MyResponseData;
import invertemotech.com.osellers.retrofitmodel.MyResponseImage;
import invertemotech.com.osellers.util.CallToOwner;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;

public class My_post_details extends Activity {


    Boolean isInternetPresent = false;


    // Connection detector class
    ConnectionDetector cd;

    Button postBtn;

    Context context;
    int success;
    String message;

    Bitmap bitmapForZoom;

    JSONObject json;


    ImageView priceImage;


    SharePref sharePref;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    String name;
    String areaId;
    String email;
    String contactnum;
    String address;
    String productname;
    String catId;
    String brandId;
    String expirydate;
    String description;
    String advertiseDate;
    String status;
    String uploadId;
    String price;
    String areaName;
    String ad_id;
    String districtName;
    ArrayList<MyResponseImage> images = new ArrayList<>();
    Matrix matrix = new Matrix();
    String districtId;


    TextView titleTV, timeTV, priceTV, descriptionTV, uploaderNameTV, contactTV, locationTV, smsTv, emailTv;

    RecyclerView imageList;

    int position;

    Button nextBtn,prevBtn;
    MyResponseData adDetails;


    ArrayList<MyResponseData> adDetailsArrayList;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    //SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy | hh:mm a");
    SimpleDateFormat sdf1 = new SimpleDateFormat("MMMM d, yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 

        setContentView(R.layout.activity_mypost_details);

        position = getIntent().getIntExtra("position",0);

        adDetailsArrayList = Parcels.unwrap(getIntent().getParcelableExtra("ad"));

        titleTV = (TextView) findViewById(R.id.title);
        timeTV = (TextView) findViewById(R.id.time);
        priceTV = (TextView) findViewById(R.id.price);
        descriptionTV = (TextView) findViewById(R.id.description);
        uploaderNameTV = (TextView) findViewById(R.id.uploaderName);
        contactTV = (TextView) findViewById(R.id.contact);
        locationTV = (TextView) findViewById(R.id.location);

        nextBtn = (Button) findViewById(R.id.nextBtn);
        prevBtn = (Button) findViewById(R.id.prevBtn);

        smsTv = (TextView) findViewById(R.id.sms);
        emailTv = (TextView) findViewById(R.id.email);
        ;
        sharePref = new SharePref(getApplicationContext());
//        priceImage = (ImageView) findViewById(R.id.priceImage);

        context = My_post_details.this;
 

        progressSweetAlertDialog = new SweetAlertDialog(My_post_details.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);



        imageList = (RecyclerView) findViewById(R.id.imageList);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);


        imageList.setLayoutManager(layoutManager);


        setData(position);

      //  Log.e("pos",position+"");

        ImageButton bckBtn = (ImageButton) findViewById(R.id.menu);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                position++;
                if(adDetailsArrayList.size()>position) {

                    //setData(position);
                    Parcelable listParcelable = Parcels.wrap(adDetailsArrayList);

                    Intent intent = new Intent(My_post_details.this, My_post_details.class);
                    intent.putExtra("ad",listParcelable);
                    intent.putExtra("position",position);
                    startActivity(intent);
                    finish();

                }else{
                    position--;
                    Toast.makeText(context, "No More Ads.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position--;
                if(position<0) {
                    position++;
                    Toast.makeText(context, "No Previous Ads.", Toast.LENGTH_SHORT).show();

                }else{

                    // setData(position);
                    Parcelable listParcelable = Parcels.wrap(adDetailsArrayList);

                    Intent intent = new Intent(My_post_details.this, My_post_details.class);
                    intent.putExtra("ad",listParcelable);
                    intent.putExtra("position",position);
                    startActivity(intent);
                    finish();
                }

            }
        });


    }




    public void setData(int position){


        uploaderNameTV.setText("");
        //contactTV.setText(contactnum);

        timeTV.setText("");
        titleTV.setText("");
        locationTV.setText("" + ", " + "");
        priceTV.setText("");
        descriptionTV.setText("");
        uploaderNameTV.setText("");

        adDetails = adDetailsArrayList.get(position);

        name = adDetails.getName();

        areaId = adDetails.getAreaId();


        email = adDetails.getEmail();
        contactnum = adDetails.getContactnum();
        address = adDetails.getDivisionName();
        productname = adDetails.getProductname();
        catId = adDetails.getCatId();
        brandId = adDetails.getBrandId();;
        expirydate = adDetails.getExpirydate();;
        description = adDetails.getDescription();
        advertiseDate = adDetails.getAdvertiseDate();
        status =adDetails.getStatus();
        uploadId =adDetails.getUploadId();
        price = adDetails.getPrice();
        districtId =adDetails.getDistrictId();
        images = adDetails.getImages();
        areaName = adDetails.getAreaName();
        districtName =adDetails.getDistrict();

        cd = new ConnectionDetector(context);



        isInternetPresent = cd.isConnectingToInternet();


//		displayImageOptions = new DisplayImageOptions.Builder()
//				.cacheInMemory(true)
//				.cacheOnDisk(true)
//				.build();
//		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
//				.defaultDisplayImageOptions(displayImageOptions)
//				.build();
//		ImageLoader.getInstance().init(config);
        String date = "";



        try {
            date = sdf1.format(sdf.parse(advertiseDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (uploadId.equals(sharePref.getshareprefdatastring(SharePref.USERID))) {

        }


        uploaderNameTV.setText(uploadId);
        //contactTV.setText(contactnum);
        descriptionTV.setText(description);

        timeTV.setText(date);
        titleTV.setText(productname);
        locationTV.setText(areaName + ", " + districtName);
        priceTV.setText("TK " + price);

//        int width = priceTV.getWidth();
//
//        priceImage.getLayoutParams().width = width;
//
//        priceImage.requestLayout();

        uploaderNameTV.setText(name);

        if (images.size() != 0) {

            String imageLink = images.get(0).toString();
        }

        Log.e("con",contactnum);
        contactTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallToOwner call = new CallToOwner(My_post_details.this, contactnum);
                call.call();
            }
        });

        emailTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{email});

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        smsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
                        + contactnum)));
            }
        });

        //ImageLoader.getInstance().displayImage(imageLink, imageView, displayImageOptions);

        if (isInternetPresent) {


            //new AsyncTaskList().execute();

        } else {
            Toast.makeText(context, "No internet connection available",
                    Toast.LENGTH_LONG).show();
        }



        MyPostDetailsImageAdapter postDetailsImageAdapter = new MyPostDetailsImageAdapter( images, context);


        imageList.setAdapter(postDetailsImageAdapter);


    }




    public static Bitmap getBitmapFromURL(String src) {


        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
package invertemotech.com.osellers.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ImagePickerActivity;
import com.esafirm.imagepicker.model.Image;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.adapter.AddImageAdapter;
import invertemotech.com.osellers.adapter.LocationAdapter;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.retrofitmodel.Category;
import invertemotech.com.osellers.retrofitmodel.CategoryResponse;
import invertemotech.com.osellers.retrofitmodel.Division;
import invertemotech.com.osellers.retrofitmodel.UpdateResponse;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NewPostActivity extends AppCompatActivity {

    int RESPONSE_CODE_LOCATION = 123;
    int RESPONSE_CODE_CATEGORY = 321;
    int REQUEST_CODE_PICKER = 420;
    static String filePath="";
    private Uri mImageUri;
    ArrayList<String> images;

    Spinner brandSpinner;

    ConnectionDetector cd;
    boolean isInternetON;
    AddImageAdapter addImageAdapter;
    ListView imageList;

    Button addImage;

    ListView categoryList;
    TextView locstionTitle;

    ArrayList<Boolean> checkList;
    ArrayList<String>  brandName;

    String userEmail,userID,userContact,username;
    Context context;

    ArrayList<Division> divisionsArrayList;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    Button done;
    TextView title1,title2,title3;
    String selectedDivision="0",selectedDistrict="0" ;
    int divisionPosition,districtPosition,areaPosition;

    String districtName="";
    String divisionName="";
    String areaName="";
    SharePref sharePref;

    ArrayList<Category> categoryArraylist;
    String selectedCategory="0",selectedSubCategory="0",selectedArea="0";
    int categoryPosition,subCategoryPosition;

    String categoryName="";
    String subCategoryName="";

    TextView loationBtn;


    ArrayList<File> finalImages;


    EditText prodcutNameEdt,productPriceEdt,addressEdt,descriptionEdt;
    String prodcutNameString,productPriceString,addressString,descriptionString,selectedBrand="";

    Button submitBtn;
    String userName;

    ArrayList imagePaths = new ArrayList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        context = NewPostActivity.this;
        cd = new ConnectionDetector(context);
        isInternetON = cd.isConnectingToInternet();
        addImage= (Button) findViewById(R.id.addImage);
        imageList= (ListView) findViewById(R.id.list);
        images = new ArrayList();
        sharePref = new SharePref(context);

        userName = sharePref.getshareprefdatastring(SharePref.USERNAME);

        prodcutNameEdt = (EditText) findViewById(R.id.productName);
        productPriceEdt = (EditText) findViewById(R.id.price);
        descriptionEdt = (EditText) findViewById(R.id.description);
        addressEdt = (EditText) findViewById(R.id.address);


        finalImages = new ArrayList<>();
        submitBtn = (Button) findViewById(R.id.submit);
        categoryList = (ListView) findViewById(R.id.categorylist);

        done = (Button) findViewById(R.id.done);
        title1 = (TextView) findViewById(R.id.title1);
        title2 = (TextView) findViewById(R.id.title2);
        title3 = (TextView) findViewById(R.id.title3);
        brandSpinner = (Spinner) findViewById(R.id.brandSpinner);
        loationBtn = (TextView) findViewById(R.id.locationBtn);
        context = NewPostActivity.this;

        addImageAdapter = new AddImageAdapter(getApplicationContext(),images);

        imageList.setAdapter(addImageAdapter);

        imagePaths = new ArrayList();


        userEmail= sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        userID= sharePref.getshareprefdatastring(SharePref.USERID);
        userContact= sharePref.getshareprefdatastring(SharePref.USERPHONE);
        username= sharePref.getshareprefdatastring(SharePref.USERNAME);


        addImageAdapter = new AddImageAdapter(getApplicationContext(),imagePaths);
        imageList.setAdapter(addImageAdapter);


        ImageButton bckBtn = (ImageButton) findViewById(R.id.menu);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (CropImage.isExplicitCameraPermissionRequired(getApplicationContext())) {
//                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
//                } else {
//                    CropImage.startPickImageActivity(PostActivity.this);
//                }

                if(imagePaths.size()<3) {

                    Intent intent = new Intent(context, ImagePickerActivity.class);

                    intent.putExtra(ImagePicker.EXTRA_FOLDER_MODE, true);
                    intent.putExtra(ImagePicker.EXTRA_MODE, ImagePicker.MODE_SINGLE);
                    intent.putExtra(ImagePicker.EXTRA_LIMIT, 1);
                    intent.putExtra(ImagePicker.EXTRA_SHOW_CAMERA, true);
                    intent.putExtra(ImagePicker.EXTRA_SELECTED_IMAGES, images);
                    intent.putExtra(ImagePicker.EXTRA_FOLDER_TITLE, "Album");
                    intent.putExtra(ImagePicker.EXTRA_IMAGE_TITLE, "Tap to select images");
                    intent.putExtra(ImagePicker.EXTRA_IMAGE_DIRECTORY, "Camera");
                    intent.putExtra(ImagePicker.EXTRA_RETURN_AFTER_FIRST, true); //default is false

                    startActivityForResult(intent, REQUEST_CODE_PICKER);
                }else{

                    Toast.makeText(NewPostActivity.this, "You can add max 3 images", Toast.LENGTH_LONG).show();
                }


            }
        });


        progressSweetAlertDialog = new SweetAlertDialog(NewPostActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);



        title1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        title1.setTextColor(getResources().getColor(R.color.whitetext));


        brandSpinner.setVisibility(View.GONE);
        title1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedCategory = "0";
                categoryPosition = 0;
                selectedSubCategory = "0";
                subCategoryPosition =0;
                categoryList.setVisibility(View.VISIBLE);
                title2.setVisibility(View.GONE);
                title3.setVisibility(View.GONE);


                title1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                title1.setTextColor(getResources().getColor(R.color.whitetext));

                ArrayList<String> locationName = new ArrayList();

                for(int i=0;i<categoryArraylist.size();i++){
                    locationName.add(categoryArraylist.get(i).getCatname());
                }

                LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                categoryList.setAdapter(locationAdapter);
                CategoryActivity.Utility.setListViewHeightBasedOnChildren(categoryList);
            }
        });
        title2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                title2.setTextColor(getResources().getColor(R.color.whitetext));
                title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                title1.setTextColor(getResources().getColor(R.color.graydark));
                selectedSubCategory = "0";
                subCategoryPosition =0;

                categoryList.setVisibility(View.VISIBLE);
                title3.setVisibility(View.GONE);
                ArrayList<String>  locationName = new ArrayList();
                for(int i=0;i<categoryArraylist.get(categoryPosition).getSubcategories().size();i++){

                    locationName.add(categoryArraylist.get(categoryPosition).getSubcategories().get(i).getSubcatagoryname());
                }

                LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                categoryList.setAdapter(locationAdapter);

                CategoryActivity.Utility.setListViewHeightBasedOnChildren(categoryList);
            }
        });


        getCategory();





        loationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetON) {

                    Intent intent = new Intent(context, LocationActivity.class);


                    startActivityForResult(intent, RESPONSE_CODE_LOCATION);
                }else {
                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();

                }
            }
        });


        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(selectedCategory.equals("0")){
                    selectedCategory = categoryArraylist.get(position).getCatid();
                    categoryName = categoryArraylist.get(position).getCatname();
                    categoryPosition = position;
                    title2.setText(categoryArraylist.get(position).getCatname());
                    title2.setVisibility(View.VISIBLE);
                    categoryList.setVisibility(View.VISIBLE);

                    title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title1.setTextColor(getResources().getColor(R.color.graydark));

                    title2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    title2.setTextColor(getResources().getColor(R.color.whitetext));


                    title3.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title3.setTextColor(getResources().getColor(R.color.graydark));


                    ArrayList<String>  locationName = new ArrayList();
                    for(int i=0;i<categoryArraylist.get(position).getSubcategories().size();i++){

                        locationName.add(categoryArraylist.get(position).getSubcategories().get(i).getSubcatagoryname());
                    }

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    categoryList.setAdapter(locationAdapter);
                    CategoryActivity.Utility.setListViewHeightBasedOnChildren(categoryList);
                }else if(!selectedCategory.equals("0") && selectedSubCategory.equals("0")){


                    title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title1.setTextColor(getResources().getColor(R.color.graydark));

                    selectedSubCategory = categoryArraylist.get(categoryPosition).getSubcategories().get(position).getSubcatid();
                    subCategoryName = categoryArraylist.get(categoryPosition).getSubcategories().get(position).getSubcatagoryname();
                    subCategoryPosition = position;
                    title3.setText(subCategoryName);
                    title3.setVisibility(View.VISIBLE);
                    categoryList.setVisibility(View.GONE);

                    ArrayList<String>  locationName = new ArrayList();

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    categoryList.setAdapter(locationAdapter);

                    ;
                    title3.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    title3.setTextColor(getResources().getColor(R.color.whitetext));


                    title2.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title2.setTextColor(getResources().getColor(R.color.graydark));


                    brandName = new ArrayList();
                    checkList =  new ArrayList<Boolean>();

                    int size =categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().size();

                    Log.e("s",size+"");
                    if(size!=0){

                        for(int i=0;i<size;i++){


                            brandName.add(categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().get(i).getBrandname());

                            checkList.add(false);

                        }

                        brandSpinner.setVisibility(View.VISIBLE);

                        brandName.add(0,"Select A Brand");
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.spinner_item,brandName);
                        brandSpinner.setAdapter(adapter);
                    }else{

                        brandSpinner.setVisibility(View.GONE);
                    }


                }
            }
        });


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetON){
                    if( nullCheck()) {
                        if (brandSpinner.getVisibility() == View.VISIBLE) {
                            if (brandSpinner.getSelectedItemPosition() != 0) {
                                selectedBrand = getSelectedBrandId(brandSpinner.getSelectedItemPosition() - 1);

                                submitBtn.setEnabled(false);
                                postAd();
                            } else {
                                submitBtn.setEnabled(true);
                                new SweetAlertDialog(NewPostActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("Select A brand")
                                        .show();
                            }
                        }else {
                            submitBtn.setEnabled(false);
                            postAd();

                        }
                    }
                }
            }
        });

    }


    public String getSelectedBrandId (int position){

        String brandId="";

         brandId = categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().get(position).getBrandId();

        Log.e("brandid",brandId);

        return brandId;
    }

    public boolean nullCheck() {
        boolean flag = false;

        String msg = "";

        if (!prodcutNameEdt.getText().toString().trim().equalsIgnoreCase("")) {
            if (!productPriceEdt.getText().toString().trim().equalsIgnoreCase("")) {
                if (!descriptionEdt.getText().toString().trim().equalsIgnoreCase("")) {

                        if (!selectedArea.equalsIgnoreCase("0")) {
                            return true;
                        } else {
                            msg = "Please Select A Area!";
                        }


                }

                else {
                    msg = "Please Confirm Password!";
                }

            }  else {
                msg = "Please Enter Product Price!";
            }

        } else {
            msg = "Please Enter Product Name!";
        }


        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {

        String text;
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            text = cursor.getString(column_index);
//            return cursor.getString(column_index);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e){

            text = contentUri.toString().replace("file://","");
        }
        return  text;

    }

    public static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }




    public static void copyFileOrDirectory(String srcDir, String dstDir) {

        try {

          //  RandomStringUtils.randomAlphanumeric(17).toUpperCase();
            String name = getSaltString();

            File src = new File(srcDir);
            File dst = new File(dstDir,name);

            if (src.isDirectory()) {

                String files[] = src.list();
                int filesLength = files.length;
                for (int i = 0; i < filesLength; i++) {
                    String src1 = (new File(src, files[i]).getPath());
                    String dst1 = dst.getPath();
                    copyFileOrDirectory(src1, dst1);

                }
            } else {
                copyFile(src, dst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
                filePath = destFile.getPath();
            }
        }
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
           ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);

            Log.e("p",images.get(0).getPath());

            for(int i=0;i<images.size();i++){

                imagePaths.add(images.get(i).getPath());
            }


            addImageAdapter.notifyDataSetChanged();
            Utility.setListViewHeightBasedOnChildren(imageList);

            addImageAdapter.setCustomObjectListener(new AddImageAdapter.onItemClickListener() {
                @Override
                public void onItemClicked(int position) {
                    imagePaths.remove(position);
                    addImageAdapter.notifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(imageList);
                }
            });

        }



        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            Log.e("uriA",imageUri+"");


            String path = getRealPathFromUri(getApplicationContext(),imageUri);

            Log.e("path",path);

            File imgFile = new File(path+"");



            if(imgFile.exists()) {
               // String root = Environment.getExternalStorageDirectory().toString()+"/osellers";
                String root =  getCacheDir().toString();

                copyFileOrDirectory(path,root);
                Log.e("filePath",filePath);
                images.add(filePath);

    }else{
       Log.e("notexpst",imageUri+"");
    }
         //   Log.e("bitmap",uriToBitmap(imageUri)+"");




        } if(requestCode==RESPONSE_CODE_LOCATION)
        {
            try {
                //String message=data.getStringExtra("location");
                divisionName = data.getStringExtra("DivisionName");
                districtName = data.getStringExtra("DistrictName");
                areaName = data.getStringExtra("AreaName");
                selectedDistrict = data.getStringExtra("DistrictId");
                selectedDivision = data.getStringExtra("DivisionId");
                selectedArea = data.getStringExtra("AreaId");

                divisionPosition = data.getIntExtra("division",200);
                districtPosition = data.getIntExtra("district",200);
                areaPosition = data.getIntExtra("area",200);

                if(!selectedArea.equals("0")){
                    loationBtn.setText(areaName);
                }

//
//                if (!areaName.equals("")) {
//                    locationName.setText(areaName);
//                } else if (!districtName.equals("")) {
//                    locationName.setText(districtName);
//                } else if (!divisionName.equals("")) {
//                    locationName.setText(divisionName);
//                }
//
            }catch (Exception e){
                e.printStackTrace();
            }


        }

    }

    private Bitmap uriToBitmap(Uri selectedFileUri) {
       Bitmap bitmap = null;
        try {
            ParcelFileDescriptor parcelFileDescriptor =
                    getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
              bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);



            parcelFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // mCurrentFragment.setImageUri(mCropImageUri);

//                Log.e("uri",imageUri+"");
//                images.add(imageUri);
//                imageList.deferNotifyDataSetChanged();

            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static class Utility {
        public static void setListViewHeightBasedOnChildren(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }




    public void getCategory(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);




        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<CategoryResponse> call = apiService.getCatgoryList();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse>call, Response<CategoryResponse> response) {


                //  topicsDataArrayList.clear();

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");
                categoryArraylist = response.body().getData();

                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){



                    ArrayList<String> locationName = new ArrayList();

                    for(int i=0;i<categoryArraylist.size();i++){
                        locationName.add(categoryArraylist.get(i).getCatname());
                    }

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    categoryList.setAdapter(locationAdapter);
                    NewPostActivity.Utility.setListViewHeightBasedOnChildren(categoryList);
                    progressSweetAlertDialog.dismiss();

                    title1.getParent().requestChildFocus(title1,title1);


                }else {

                }



                // copyToDatabase(  categories,  topics,  questions,tests);

                progressSweetAlertDialog.dismiss();
//                Intent in = new Intent(getActivity(), LoginActivity.class);
//                startActivity(in);




            }

            @Override
            public void onFailure(Call<CategoryResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());

                progressSweetAlertDialog.dismiss();
            }
        });


    }



    public void postAd(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

          prodcutNameString = prodcutNameEdt.getText().toString();
        productPriceString = productPriceEdt.getText().toString();
        addressString = addressEdt.getText().toString();
        descriptionString = descriptionEdt.getText().toString();




        HashMap<String, RequestBody> map=new HashMap<>(imagePaths.size());
        RequestBody file=null;
        File f=null;

        for(int i=0,size=imagePaths.size(); i<size;i++){



            File imgFile = new File(imagePaths.get(i).toString());
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imagePaths.get(i).toString() + "");

                Bitmap watermarkBitmap = mark(myBitmap,"osellers.com");


//                  f = new File(getCacheDir(), imagePaths.get(i).toString());
//                if (!f.exists()) {
//
//                    try {
//                        f.createNewFile();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }

                try {
                    String rootPath = Environment.getExternalStorageDirectory()
                            .getAbsolutePath() + "/oseller/";
                    File root = new File(rootPath);
                    if (!root.exists()) {
                        root.mkdirs();
                    }

                    String name = imagePaths.get(i).toString();

                    int s =imagePaths.get(i).toString().lastIndexOf('/');
                    name= name.substring(s+1,name.length());
                    name =  Calendar.getInstance().getTimeInMillis()+name.replace(" ","");
                    f = new File(rootPath + name);
                    if (f.exists()) {
                        f.delete();
                    }
                    int permissionCheck = ContextCompat.checkSelfPermission(NewPostActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                       Log.e("path", f.getPath());
                        f.createNewFile();
                    }

                    FileOutputStream out = new FileOutputStream(f);

                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    submitBtn.setEnabled(true);
                }


//Convert bitmap to byte array
                Bitmap bitmap = watermarkBitmap;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos);

                byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(f);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                finalImages.add(f);
            }





        }


        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[finalImages.size()];

        for (int index = 0; index < finalImages.size(); index++) {
        //    Log.d("AS", "requestUploadSurvey: survey image " + index + "  " + finalImages.get(index).get());

            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), finalImages.get(index));
            surveyImagesParts[index] = MultipartBody.Part.createFormData("files[]", finalImages.get(index).getName().replace(" ",""), surveyBody);
        }




        Log.e("error",surveyImagesParts.toString()+"");




        Call<UpdateResponse> call = apiService.postAdd(prodcutNameString,selectedSubCategory,
                selectedBrand,descriptionString,Float.parseFloat(productPriceString),userName,selectedArea,userEmail,userContact,addressString,surveyImagesParts);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){

                    submitBtn.setEnabled(true);
                    progressSweetAlertDialog.dismiss();


                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText(msg);
                    doneDialog.setConfirmText("Ok");
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();

                            finish();
                        }
                    });
                    doneDialog.show();




                }else {
                    progressSweetAlertDialog.dismiss();
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText("ERROR");
                    doneDialog.setContentText(msg);
                    doneDialog.show();
                    submitBtn.setEnabled(true);
                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                submitBtn.setEnabled(true);
                progressSweetAlertDialog.dismiss();
            }
        });


    }


    public static Bitmap mark(Bitmap src, String watermark) {

        int height=800, width=800;

        Bitmap background = Bitmap.createBitmap((int)800, (int)800, Bitmap.Config.ARGB_8888);

        float originalWidth = src.getWidth();
        float originalHeight = src.getHeight();

        Canvas canvas = new Canvas(background);

        float scale = width / originalWidth;

        float xTranslation = 0.0f;
        float yTranslation = (height - originalHeight * scale) / 2.0f;

        Matrix transformation = new Matrix();
        transformation.postTranslate(xTranslation, yTranslation);
        transformation.preScale(scale, scale);

        Paint paint = new Paint();
        paint.setFilterBitmap(true);

        canvas.drawBitmap(src, transformation, paint);

        int w = background.getWidth();
        int h = background.getHeight();

        Bitmap result = Bitmap.createBitmap(w, h, background.getConfig());
        Canvas canvas1 = new Canvas(result);
        canvas1.drawBitmap(background, 0, 0, null);
        Paint paint1 = new Paint();
        paint1.setColor(Color.RED);
        paint1.setTextSize(40);
        paint1.setAntiAlias(true);
        paint1.setUnderlineText(true);
        canvas1.drawText(watermark, w/3,h/2, paint1);


        return result;
    }


}

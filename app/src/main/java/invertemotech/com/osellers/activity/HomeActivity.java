package invertemotech.com.osellers.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import invertemotech.com.osellers.R;
import invertemotech.com.osellers.adapter.NavDrawerListAdapter;
import invertemotech.com.osellers.fragment.Fragment_home;
import invertemotech.com.osellers.fragment.ProfileFragment;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;


public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemClickListener , Animation.AnimationListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ImageView mDrawerfooter;
    private ImageButton mDrawerToggle, myProfile, addPoll,notification;

    public int newMessageCounter = 0;

    TextView changePicture;

  SharePref sharePref;
    boolean isInternetOn;
   NavDrawerListAdapter adapter;
    private ArrayList<String> navDrawerItems;
    private Uri mCropImageUri;
    int userid;
  ConnectionDetector cd;
    private ArrayList<Integer> navDrawerItemsImages;
    // Animation
    Animation animRotate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharePref = new SharePref(getApplicationContext());


        String lg = sharePref.getshareprefdatastring(SharePref.LANGUAGE);
        Log.e("set",lg);




       setContentView(R.layout.activity_home);



        animRotate = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rotate);

        // set animation listener
        animRotate.setAnimationListener(this);

        cd = new ConnectionDetector(getApplicationContext());


        isInternetOn = cd.isConnectingToInternet();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerToggle = (ImageButton) findViewById(R.id.menu);
        myProfile = (ImageButton) findViewById(R.id.profile);
        addPoll = (ImageButton) findViewById(R.id.addPoll);
        notification = (ImageButton) findViewById(R.id.notification);

        sharePref = new SharePref(getApplicationContext());




        Log.e("currentid", userid + "");
        navDrawerItems = new ArrayList<>();
        navDrawerItemsImages = new ArrayList<>();
        navDrawerItems.add("");
        navDrawerItems.add("");
        navDrawerItems.add("All Ads");
        navDrawerItems.add("Post Your Ad");

        navDrawerItems.add("Profile");;

        navDrawerItems.add("About Us");;
        navDrawerItems.add("Settings");

//        navDrawerItems.add(getResources().getString (R.string.all_ads));
//        navDrawerItems.add(getResources().getString (R.string.post_add));
//
//        navDrawerItems.add(getResources().getString (R.string.my_profile));;
//
//        navDrawerItems.add(getResources().getString (R.string.about_us));;
//        navDrawerItems.add(getResources().getString (R.string.settings));


        navDrawerItemsImages.add(0);
        navDrawerItemsImages.add(0);

        navDrawerItemsImages.add(R.drawable.ic_search);
        navDrawerItemsImages.add(R.drawable.ic_add);

        navDrawerItemsImages.add(R.drawable.ic_profile);

        navDrawerItemsImages.add(R.drawable.ic_flame);
        navDrawerItemsImages.add(R.drawable.ic_settings);


        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems, navDrawerItemsImages);





        mDrawerList.setAdapter(adapter);







        mDrawerToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }

                mDrawerToggle.startAnimation(animRotate);
            }
        });









        LoadHome();
        mDrawerList.setOnItemClickListener(this);

    }

    @Override
    protected void onResume() {
        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems, navDrawerItemsImages);


        mDrawerList.setAdapter(adapter);

        super.onResume();



    }

    @Override
    protected void onDestroy() {
        if(isInternetOn){

        }
        super.onDestroy();
    }





    public void LoadHome() {

        Fragment fragment = new Fragment_home();
        FragmentManager mFragmentManager = getSupportFragmentManager();

        FragmentTransaction ft = mFragmentManager.beginTransaction();


        ft.replace(R.id.frame_container, fragment);

        ft.commit();

    }


    public void setBangla(){

        String languageToLoad  = "bn"; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_home);


    }

    public void setEnglish(){

        String languageToLoad  = "en"; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_home);

    }
    public void displayView(int position) {



        switch (position) {
            case 0:

                break;
            case 1:

                String link= "https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName();

                        Intent intent6 = new Intent();
                intent6.setAction(Intent.ACTION_SEND);

                intent6.setType("text/plain");
                intent6.putExtra(Intent.EXTRA_TEXT, link);
                startActivity(Intent.createChooser(intent6, "Share"));

                break;


            case 2:
              LoadHome();

                break;
            case 5:


                break;
            case 3:

                if(sharePref.getshareprefdataBoolean(SharePref.LOGEDIN)){

                    Intent intent=new Intent(HomeActivity.this,NewPostActivity.class);
                    startActivity(intent);

                }else{

                    Intent i = new Intent(HomeActivity.this,LoginActivity.class);
                    startActivity(i);

                }

                break;
            case 4:

                if(sharePref.getshareprefdataBoolean(SharePref.LOGEDIN)){

                    Fragment fragment = new ProfileFragment();
                    FragmentManager mFragmentManager = getSupportFragmentManager();

                    FragmentTransaction ft = mFragmentManager.beginTransaction();


                    ft.replace(R.id.frame_container, fragment);

                    ft.commit();

                }else{

                    Intent i = new Intent(HomeActivity.this,LoginActivity.class);
                    startActivity(i);

                }


                break;
            case 6:

                Intent i = new Intent(HomeActivity.this,SettingsActivity.class);
                startActivity(i);
                break;
            case 7:




                break;
            case 8:



                break;

            case 9:


                break;


            default:
                break;
        }


        mDrawerList.setSelection(position);

        mDrawerLayout.closeDrawer(mDrawerList);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        // Toast.makeText(getApplicationContext(),"clciked "+i,Toast.LENGTH_LONG).show();
        displayView(i);
    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

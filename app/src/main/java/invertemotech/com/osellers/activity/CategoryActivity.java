package invertemotech.com.osellers.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.adapter.LocationAdapter;
import invertemotech.com.osellers.adapter.SpinnerAdapter;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.retrofitmodel.Category;
import invertemotech.com.osellers.retrofitmodel.CategoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends AppCompatActivity {
    ArrayList<Boolean> checkList;
    ArrayList<String>  brandName;
    ListView list;
    TextView locstionTitle;

    Context context;

    ArrayList<Category> categoryArraylist;
    ListView locations;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    Button done;
    TextView title1,title2,title3,title4;
    String selectedCategory="0",selectedSubCategory="0",selectedArea="0";
    int categoryPosition,subCategoryPosition;

    String categoryName="";
    String subCategoryName="";

    TextView brandSpinner;

    String selectedBrandsId="0";

    LinearLayout filterLayout,brandLayout;
    SpinnerAdapter spinnerAdapter;

    String price1,price2;
    EditText price1Edt,price2Edt;
    boolean hasSetCategory = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
       overridePendingTransition(R.anim.slidein_from_bottom, R.anim.nothing);

        locations = (ListView) findViewById(R.id.list);

        done = (Button) findViewById(R.id.done);
        title1 = (TextView) findViewById(R.id.title1);
        title2 = (TextView) findViewById(R.id.title2);
        title3 = (TextView) findViewById(R.id.title3);
        title4 = (TextView) findViewById(R.id.title4);

        brandSpinner = (TextView) findViewById(R.id.brands);
        price1Edt = (EditText) findViewById(R.id.price1);
        price2Edt = (EditText) findViewById(R.id.price2);


        checkList = (ArrayList<Boolean>) getIntent().getSerializableExtra("brandPosition");
        subCategoryPosition = getIntent().getIntExtra("subcateogryPosition",200);
        categoryPosition = getIntent().getIntExtra("categoryPosition",200);
        price1 = getIntent().getStringExtra("price1");
        price2 = getIntent().getStringExtra("price2");
        hasSetCategory = getIntent().getBooleanExtra("setCategory",false);
        context = CategoryActivity.this;

        progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        filterLayout = (LinearLayout) findViewById(R.id.filterLayout);
        brandLayout = (LinearLayout) findViewById(R.id.brandLayout);

        title1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        title1.setTextColor(getResources().getColor(R.color.whitetext));


        price1Edt.setText(price1);
        price2Edt.setText(price2);
        title1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedCategory = "0";
                categoryPosition = 0;
                selectedSubCategory = "0";
                subCategoryPosition =0;
                locations.setVisibility(View.VISIBLE);
                title2.setVisibility(View.GONE);
                title3.setVisibility(View.GONE);
                title4.setVisibility(View.GONE);
                filterLayout.setVisibility(View.GONE);


                title1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                title1.setTextColor(getResources().getColor(R.color.whitetext));

                ArrayList<String> locationName = new ArrayList();

                for(int i=0;i<categoryArraylist.size();i++){
                    locationName.add(categoryArraylist.get(i).getCatname());
                }

                LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                locations.setAdapter(locationAdapter);
                CategoryActivity.Utility.setListViewHeightBasedOnChildren(locations);
            }
        });
      title2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                title2.setTextColor(getResources().getColor(R.color.whitetext));
                title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                title1.setTextColor(getResources().getColor(R.color.black));
                selectedSubCategory = "0";
                subCategoryPosition =0;
                selectedArea = "0";
                locations.setVisibility(View.VISIBLE);
                filterLayout.setVisibility(View.GONE);
                title3.setVisibility(View.GONE);
                title4.setVisibility(View.GONE);
                ArrayList<String>  locationName = new ArrayList();
                for(int i=0;i<categoryArraylist.get(categoryPosition).getSubcategories().size();i++){

                    locationName.add(categoryArraylist.get(categoryPosition).getSubcategories().get(i).getSubcatagoryname());
                }

                LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                locations.setAdapter(locationAdapter);

                CategoryActivity.Utility.setListViewHeightBasedOnChildren(locations);
            }
        });


        getCategory();





        brandSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showSpinnerDialog();
            }
        });


        locations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(selectedCategory.equals("0")){
                    selectedCategory = categoryArraylist.get(position).getCatid();
                    categoryName = categoryArraylist.get(position).getCatname();
                    categoryPosition = position;
                    title2.setText(categoryArraylist.get(position).getCatname());
                    title2.setVisibility(View.VISIBLE);
                    locations.setVisibility(View.VISIBLE);
                    filterLayout.setVisibility(View.GONE);

                    title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title1.setTextColor(getResources().getColor(R.color.black));

                    title2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    title2.setTextColor(getResources().getColor(R.color.whitetext));


                    title3.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title3.setTextColor(getResources().getColor(R.color.black));


                    ArrayList<String>  locationName = new ArrayList();
                    for(int i=0;i<categoryArraylist.get(position).getSubcategories().size();i++){

                        locationName.add(categoryArraylist.get(position).getSubcategories().get(i).getSubcatagoryname());
                    }

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    locations.setAdapter(locationAdapter);
                    CategoryActivity.Utility.setListViewHeightBasedOnChildren(locations);
                }else if(!selectedCategory.equals("0") && selectedSubCategory.equals("0")){


                    title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title1.setTextColor(getResources().getColor(R.color.black));

                    selectedSubCategory = categoryArraylist.get(categoryPosition).getSubcategories().get(position).getSubcatid();
                    subCategoryName = categoryArraylist.get(categoryPosition).getSubcategories().get(position).getSubcatagoryname();
                    subCategoryPosition = position;
                    title3.setText(subCategoryName);
                    title3.setVisibility(View.VISIBLE);
                    locations.setVisibility(View.GONE);

                    filterLayout.setVisibility(View.VISIBLE);

                    ArrayList<String>  locationName = new ArrayList();

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    locations.setAdapter(locationAdapter);

             ;
                    title3.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    title3.setTextColor(getResources().getColor(R.color.whitetext));


                    title2.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title2.setTextColor(getResources().getColor(R.color.black));


                    brandName = new ArrayList();
                   checkList =  new ArrayList<Boolean>();

                    int size =categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().size();

                    Log.e("s",size+"");
                    if(size!=0){

                    for(int i=0;i<size;i++){


                        brandName.add(categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().get(i).getBrandname());

                        checkList.add(false);

                    }
                        brandLayout.setVisibility(View.VISIBLE);

                    }else{

                        brandLayout.setVisibility(View.GONE);
                    }


                }
            }
        });


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hasSetCategory = true;

                Intent data = new Intent();
                if(subCategoryName.equals("")) {
                    data.putExtra("subcategoryName","");
                    data.putExtra("subcategoryId","0");

                }else {
                    data.putExtra("subcategoryName",subCategoryName);
                    data.putExtra("subcategoryId",selectedSubCategory);
                }


 

                data.putExtra("brandPosition",checkList);
                data.putExtra("subcateogryPosition",subCategoryPosition);
                data.putExtra("categoryPosition",categoryPosition);
                data.putExtra("brandId",selectedBrandsId);
                data.putExtra("setCategory",hasSetCategory);
                data.putExtra("selectedCateogry",selectedCategory);

                price1 = price1Edt.getText().toString().trim();
                price2 = price2Edt.getText().toString().trim();


                Log.e("subcateogryPosition",subCategoryPosition+",");
                Log.e("categoryPosition",categoryPosition+",");
                Log.e("p1",price1+",");
                Log.e("p2",price2+",");

                try {
                    if (Integer.parseInt(price1) > Integer.parseInt(price2)) {
                        int x = Integer.parseInt(price1);
                        int y = Integer.parseInt(price2);
                        price1 = y + "";
                        price2 = x + "";

                        Log.e("p1",price1);
                        Log.e("p2",price2);
                    }
                }catch (Exception e){

                }
                data.putExtra("price1",price1);
                data.putExtra("price2",price2);


                setResult(RESULT_OK,data);
                finish();
                overridePendingTransition(R.anim.slide_up, R.anim.slideout_to_bottom);
            }
        });

    }

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(R.anim.slide_up, R.anim.slideout_to_bottom);

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {



        super.onDestroy();
    }


    public void getCategory(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);




        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<CategoryResponse> call = apiService.getCatgoryList();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse>call, Response<CategoryResponse> response) {


                //  topicsDataArrayList.clear();

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");
                categoryArraylist = response.body().getData();

                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){



                    ArrayList<String> locationName = new ArrayList();

                    for(int i=0;i<categoryArraylist.size();i++){
                        locationName.add(categoryArraylist.get(i).getCatname());
                    }

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    locations.setAdapter(locationAdapter);
                    CategoryActivity.Utility.setListViewHeightBasedOnChildren(locations);
                    progressSweetAlertDialog.dismiss();

                    if(hasSetCategory){

                        setPreviousData();
                    }


                }else {

                }



                // copyToDatabase(  categories,  topics,  questions,tests);

                progressSweetAlertDialog.dismiss();
//                Intent in = new Intent(getActivity(), LoginActivity.class);
//                startActivity(in);




            }

            @Override
            public void onFailure(Call<CategoryResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());

                progressSweetAlertDialog.dismiss();
            }
        });


    }


    public void setPreviousData(){


        if(categoryPosition!=200){


            selectedCategory = categoryArraylist.get(categoryPosition).getCatid();
            categoryName = categoryArraylist.get(categoryPosition).getCatname();

            title2.setText(categoryArraylist.get(categoryPosition).getCatname());
            title2.setVisibility(View.VISIBLE);
            locations.setVisibility(View.VISIBLE);
            filterLayout.setVisibility(View.GONE);

            title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title1.setTextColor(getResources().getColor(R.color.black));

            title2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            title2.setTextColor(getResources().getColor(R.color.whitetext));


            title3.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title3.setTextColor(getResources().getColor(R.color.black));

                    ArrayList<String> locationName = new ArrayList();
            for(int i=0;i<categoryArraylist.get(categoryPosition).getSubcategories().size();i++){

                locationName.add(categoryArraylist.get(categoryPosition).getSubcategories().get(i).getSubcatagoryname());
            }

            LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
            locations.setAdapter(locationAdapter);

            CategoryActivity.Utility.setListViewHeightBasedOnChildren(locations);

                     }




        if(subCategoryPosition!=200){
            title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title1.setTextColor(getResources().getColor(R.color.black));

            selectedSubCategory = categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getSubcatid();
            subCategoryName = categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getSubcatagoryname();

            title3.setText(subCategoryName);
            title3.setVisibility(View.VISIBLE);
            locations.setVisibility(View.GONE);

            filterLayout.setVisibility(View.VISIBLE);

            ArrayList<String>  locationName = new ArrayList();

            LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
            locations.setAdapter(locationAdapter);

            ;
            title3.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            title3.setTextColor(getResources().getColor(R.color.whitetext));


            title2.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title2.setTextColor(getResources().getColor(R.color.black));


            brandName = new ArrayList();


            int size =categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().size();

            Log.e("s",size+"");
            if(size!=0){

                for(int i=0;i<size;i++){


                    brandName.add(categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().get(i).getBrandname());


                }
                brandLayout.setVisibility(View.VISIBLE);

            }else{

                brandLayout.setVisibility(View.GONE);
            }


            brandSpinner.setText(brandGenerator());

        }

    }

    public static class Utility {
        public static void setListViewHeightBasedOnChildren(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }



    }


    public void showSpinnerDialog(){
        final Dialog dialog = new Dialog(this,  android.R.style.Theme_Dialog);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);

        new ContextThemeWrapper(context, android.R.style.Theme_Dialog);
        dialog.setContentView(R.layout.spinner_dialog);

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE); // for activity use context instead of getActivity()
        Display display = wm.getDefaultDisplay(); // getting the screen size of device
        Point size = new Point();
        display.getSize(size);
        int width = size.x - 20;  // Set your heights
        int height = size.y - 80; // set your widths

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());

        lp.width = width;
        lp.height = height;

        dialog.getWindow().setAttributes(lp);

        dialog.show();

        TextView title = (TextView) dialog.findViewById(R.id.title);
        Button done = (Button) dialog.findViewById(R.id.done);


        Button exit = (Button) dialog.findViewById(R.id.cancel);
        final ListView list = (ListView) dialog.findViewById(R.id.brandlist);

        spinnerAdapter = new SpinnerAdapter(getApplicationContext(),brandName,checkList);


        list.setAdapter(spinnerAdapter);

        Log.e("bS",brandName.size()+"");
        Log.e("bS",spinnerAdapter.getCount()+"");

//        ArrayList<String> locationName = new ArrayList();
//
//        for(int i=0;i<categoryArraylist.size();i++){
//            locationName.add(categoryArraylist.get(i).getCatname());
//        }
//
//        LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
//        list.setAdapter(locationAdapter);



        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String brand =brandGenerator();

                if(brand.length()>30){
                    brand = brand.substring(0,30)+"...";
                }

                if(brand.equals("")){
                    brandSpinner.setText("No Topics Selected" );
                }else {
                    brandSpinner.setText(brand);
                }

                dialog.dismiss();

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        spinnerAdapter.setCustomObjectListener(new SpinnerAdapter.onItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                Log.e("check1",checkList.get(position)+"");

                if(!checkList.get(position)){
                    checkList.set(position,true);

                }else {
                    checkList.set(position,false);
                }

                Log.e("check2",checkList.get(position)+"");
                spinnerAdapter.notifyDataSetChanged();
            }
        });

    }


    public String brandGenerator(){

        String topics="";

        for(int i=0;i<checkList.size();i++){
            if(checkList.get(i)){
                if (topics.equals("")) {
                    topics =  topics +categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().get(i).getBrandname();
                    selectedBrandsId = selectedBrandsId +categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().get(i).getBrandId();

                }else{
                    topics =  topics +", " + categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().get(i).getBrandname();
                    selectedBrandsId = selectedBrandsId +", "+categoryArraylist.get(categoryPosition).getSubcategories().get(subCategoryPosition).getBrands().get(i).getBrandId();
                }



            }
        }

        Log.e("topics",topics);
        Log.e("id",selectedBrandsId);

        if(topics.equals("")){
            topics = "All Brands";
        }

        return topics;
    }

}

package invertemotech.com.osellers.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import invertemotech.com.osellers.R;
import invertemotech.com.osellers.util.SharePref;

public class SplashActivity extends AppCompatActivity {

    SharePref sharePref;
    ImageView mImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharePref = new SharePref(getApplicationContext());



        mImageView = (ImageView) findViewById(R.id.image);


        final Animation zoomAnimation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.zoom_out);
        mImageView.startAnimation(zoomAnimation);
        zoomAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Handler mHandler = new Handler(getMainLooper());
                Runnable mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        Intent i = new Intent(SplashActivity.this,HomeActivity.class);
                        startActivity(i);

                    }
                };
                mHandler.postDelayed(mRunnable, 3000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

}







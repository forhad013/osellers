package invertemotech.com.osellers.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.adapter.MyAdsAdapter;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.retrofitmodel.MyPostResponse;
import invertemotech.com.osellers.retrofitmodel.MyResponseData;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPostsActivity extends AppCompatActivity {

    ConnectionDetector cd;
    boolean isInternetON;
    Context context;
    SharePref sharePref;
    ListView listView;
    LinearLayout emptyLayout;


    MyAdsAdapter adsAdapter;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;


    ArrayList<MyResponseData> adDetailsArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts);

        context = MyPostsActivity.this;

        sharePref = new SharePref(context);

        emptyLayout = (LinearLayout)  findViewById(R.id.empty);

        listView = (ListView)  findViewById(R.id.list);
        cd = new ConnectionDetector(context);
        isInternetON = cd.isConnectingToInternet();
        progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Parcelable listParcelable = Parcels.wrap(adDetailsArrayList);

                Intent intent = new Intent(context, My_post_details.class);
                intent.putExtra("ad",listParcelable);
                intent.putExtra("position",position);
                startActivity(intent);

            }
        });

        adDetailsArrayList = new ArrayList<>();
        if(isInternetON){
            getAds();
        }
    }

    public void getAds(){



        String email = sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        String query="";
        try {
              query = URLEncoder.encode(email, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.e("q",query);

        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<MyPostResponse> call = apiService.getMyAds(email);


        call.enqueue(new Callback<MyPostResponse>() {
            @Override
            public void onResponse(Call<MyPostResponse>call, Response<MyPostResponse> response) {



                try {


                    ArrayList<MyResponseData> ads = response.body().getData();


                    boolean error = response.body().getError();

                    String msg = response.body().getMessage();

                    Log.e("error", error + "");
                    Log.e("msg", msg + "");


                    if (!error) {
                        emptyLayout.setVisibility(View.GONE);

                        for (int i = 0; i < ads.size(); i++) {
                            adDetailsArrayList.add(ads.get(i));
                        }


                        adsAdapter = new MyAdsAdapter(context,adDetailsArrayList);
                        listView.setAdapter(adsAdapter);
//                    Log.e("a",response.body().getAdsData().getNextPageUrl());

                    } else {

                    }


                }catch (Exception e){

                    e.printStackTrace();

                    if(adDetailsArrayList.size()==0){
                        emptyLayout.setVisibility(View.VISIBLE);
                    }

                    Toast.makeText(context,"No Ad",Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<MyPostResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());

                if(adDetailsArrayList.size()==0){
                    emptyLayout.setVisibility(View.VISIBLE);
                }

                Toast.makeText(context,"No Ad",Toast.LENGTH_LONG).show();
            }
        });


    }
}

package invertemotech.com.osellers.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.retrofitmodel.UpdateResponse;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static invertemotech.com.osellers.R.id.email;
import static invertemotech.com.osellers.R.id.password1;
import static invertemotech.com.osellers.R.id.password2;

public class RegistrationActivity extends AppCompatActivity {

    String msg;

    SweetAlertDialog progressSweetAlertDialog,doneDialog;
    SharePref sharePref;
    EditText nameEdt,emailEdt,password1Edt,contactEdt,password2Edt;
    String nameString,emailString,password1String,contactString,password2String;

    Context context;
    Button signUpBtn;
    ConnectionDetector cd;
    boolean isInternetON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        context = RegistrationActivity.this;

        nameEdt = (EditText) findViewById(R.id.name);
        emailEdt = (EditText) findViewById(email);
        password1Edt = (EditText) findViewById(password1);
        password2Edt = (EditText) findViewById(password2);

        contactEdt = (EditText) findViewById(R.id.phone);

        signUpBtn = (Button) findViewById(R.id.registration);

        sharePref = new SharePref(RegistrationActivity.this);
        progressSweetAlertDialog = new SweetAlertDialog(RegistrationActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        cd = new ConnectionDetector(context);
        isInternetON = cd.isConnectingToInternet();

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nameString = nameEdt.getText().toString();
                emailString = emailEdt.getText().toString();
                password1String = password1Edt.getText().toString();
                password2String = password2Edt.getText().toString();
                contactString = contactEdt.getText().toString();

                if(nullCheck()){
                    if(password1String.equals(password2String)) {
                        if (isInternetON) {
                            register();
                        } else {
                            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
                        }
                    }else{

                        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Password doesn't match`")
                                .show();
                    }
                }

            }
        });



    }




    public boolean nullCheck() {
        boolean flag = false;



        if (!emailEdt.getText().toString().trim().equalsIgnoreCase("")) {
            if (!password1Edt.getText().toString().trim().equalsIgnoreCase("")) {
            if (!password2Edt.getText().toString().trim().equalsIgnoreCase("")) {
                    if (!contactEdt.getText().toString().trim().equalsIgnoreCase("")) {
                        if (!nameEdt.getText().toString().trim().equalsIgnoreCase("")) {
                            return true;
                        } else {
                            msg = "Please Enter Name!";
                        }
                    } else {
                        msg = "Please Enter Mobile number!";
                    }

                }

            else {
                msg = "Please Confirm Password!";
            }

        }  else {
                msg = "Please Enter Password!";
            }

        } else {
            msg = "Please Enter Email address!";
        }


        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }



    public void register(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.regitser(nameString,emailString,contactString,password1String,password2String);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){


                    sharePref.setshareprefdatastring(SharePref.USERNAME,nameString);

                    sharePref.setshareprefdatastring(SharePref.USEREMAIL,emailString+"");
                    sharePref.setshareprefdatastring(SharePref.USERPHONE,contactString+"");
                    sharePref.setshareprefdataBoolean(SharePref.LOGEDIN, false);
                    sharePref.setshareprefdatastring(SharePref.USERPASSWWORD,password1String+"");
                    progressSweetAlertDialog.dismiss();


                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText(msg);
                    doneDialog.setConfirmText("Ok");
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();

                            finish();
                        }
                    });
                    doneDialog.show();




                }else {
                    progressSweetAlertDialog.dismiss();
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText("ERROR");
                    doneDialog.setContentText(msg);
                    doneDialog.show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }
}

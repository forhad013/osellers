package invertemotech.com.osellers.activity;

import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.retrofitmodel.LoginData;
import invertemotech.com.osellers.retrofitmodel.LoginResponse;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };


    String message;
    int success;
    // UI references.
    Context context;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    String emailString,passwordString;
    private View mProgressView;
    private View mLoginFormView;
    Button mEmailSignInButton;
    String msg;
    TextView registration;

    boolean isInternetOn;
    JSONObject json;

    JSONObject jsonData;

    SharePref sharePref;

    TextView forgetPassword;
    ConnectionDetector cd;
    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        context = LoginActivity.this;
        mPasswordView = (EditText) findViewById(R.id.password);

        sharePref = new SharePref(getApplicationContext());

        cd  = new ConnectionDetector(getApplicationContext());

        isInternetOn = cd.isConnectingToInternet();

        registration = (TextView) findViewById(R.id.registration);

        forgetPassword = (TextView) findViewById(R.id.forgetPassword);


        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                emailString = mEmailView.getText().toString().trim();

                passwordString = mPasswordView.getText().toString().trim();



                if(nullCheck()){

                    isInternetOn = cd.isConnectingToInternet();

                    if(isInternetOn) {

                        login();
                      }
                    else{
                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }

            }
        });

        mLoginFormView = findViewById(R.id.login_form);



        forgetPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent1 = new Intent(getApplicationContext(), ForgetPassword.class);


  //              startActivity(intent1);
            }
        });

        progressSweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        registration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getApplicationContext(), RegistrationActivity.class);


                startActivity(intent1);
            }
        });


        mEmailView.setText(sharePref.getshareprefdatastring(SharePref.USEREMAIL));
        mPasswordView.setText(sharePref.getshareprefdatastring(SharePref.USERPASSWWORD));


    }



    public void login(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<LoginResponse> call = apiService.login(emailString,passwordString);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse>call, Response<LoginResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();
               LoginData loginData = response.body().getLoginData();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){


                    sharePref.setshareprefdatastring(SharePref.USERNAME,loginData.getUsername());

                    sharePref.setshareprefdatastring(SharePref.USEREMAIL,loginData.getUseremail()+"");
                    sharePref.setshareprefdatastring(SharePref.USERID,loginData.getOsellersUserId()+"");
                    sharePref.setshareprefdatastring(SharePref.USERPHONE,loginData.getContactnum()+"");
                    sharePref.setshareprefdataBoolean(SharePref.LOGEDIN, true);
                    sharePref.setshareprefdatastring(SharePref.USERPASSWWORD,passwordString+"");
                    progressSweetAlertDialog.dismiss();


                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText(msg);
                    doneDialog.setConfirmText("Ok");
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();

                            finish();
                        }
                    });
                    doneDialog.show();




                }else {
                    progressSweetAlertDialog.dismiss();
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText("ERROR");
                    doneDialog.setContentText(msg);
                    doneDialog.show();

                }





            }

            @Override
            public void onFailure(Call<LoginResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }

    public boolean nullCheck() {
        boolean flag = false;

        if (!mEmailView.getText().toString().trim().equalsIgnoreCase("")) {
            if (!mPasswordView.getText().toString().trim().equalsIgnoreCase("")) {

                return true;

            } else {
                msg = "Please Enter Password!";
            }

        } else {
            msg = "Please Enter Shop name!";
        }


        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView,"Need to read contacts", Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    @Override
    protected void onResume() {



        super.onResume();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */


    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }



    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
}


package invertemotech.com.osellers.activity;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import invertemotech.com.osellers.R;
import invertemotech.com.osellers.util.LocaleHelper;
import invertemotech.com.osellers.util.SharePref;

public class SettingsActivity extends AppCompatActivity   {

    Button banglaBtn,englishBtn;

    TextView dataTitle,dataInfo,languageTitle;

    SharePref sharePref;

    Switch dataSettingsBtn;

    boolean dontShowImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sharePref = new SharePref(getApplicationContext());

        dataTitle = (TextView) findViewById(R.id.dataTitle);
        dataInfo = (TextView) findViewById(R.id.dataInfo);
        languageTitle = (TextView) findViewById(R.id.languageTitle);
        banglaBtn = (Button) findViewById(R.id.bangla);
        englishBtn = (Button) findViewById(R.id.english);


        dataSettingsBtn = (Switch) findViewById(R.id.dataSave);


        dontShowImage = sharePref.getshareprefdataBoolean(SharePref.DONT_SHOW_IMAGE);


        if(dontShowImage){
            dataSettingsBtn.setChecked(true);
        }else{
            dataSettingsBtn.setChecked(false);
        }


        dataSettingsBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                sharePref.setshareprefdataBoolean(SharePref.DONT_SHOW_IMAGE,isChecked);
            }
        });


//        String lg = sharePref.getshareprefdatastring(SharePref.LANGUAGE);
//        Log.e("set",lg);
//
//        if(lg.equals("bangla")){
//            //setBangla();
//            banglaBtn.setBackground(getResources().getDrawable(R.drawable.backbround_appcolor));
//
//            englishBtn.setBackground(getResources().getDrawable(R.drawable.backbround_white_color));
//            updateViews("bn");
//        }else {
//          //  setEnglish();
//            englishBtn.setBackground(getResources().getDrawable(R.drawable.backbround_appcolor));
//
//            banglaBtn.setBackground(getResources().getDrawable(R.drawable.backbround_white_color));
//            updateViews("bn");
//        }
//
//
//
//        banglaBtn.setOnClickListener(this);
//        englishBtn.setOnClickListener(this);


    }

//
//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(LocaleHelper.onAttach(base));
//    }
    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        Resources resources = context.getResources();

        dataInfo.setText(getResources().getString(R.string.dataSettingInfo));
        dataTitle.setText(getResources().getString(R.string.dataSettingTitle));
        languageTitle.setText(getResources().getString(R.string.languageTitle));


    }

//    @Override
//    public void onClick(View v) {
//
//        switch (v.getId()) {
//            case R.id.english:
//                banglaBtn.setBackgroundResource(R.drawable.backbround_white_color);
//                englishBtn.setBackgroundResource(R.drawable.backbround_appcolor);
//
//                sharePref.setshareprefdatastring(SharePref.LANGUAGE,"english");
//
//                //   setEnglish();
//
//                String lg = sharePref.getshareprefdatastring(SharePref.LANGUAGE);
//                Log.e("set",lg);
//                break;
//            case R.id.bangla:
//
//                banglaBtn.setBackgroundResource(R.drawable.backbround_appcolor);
//                englishBtn.setBackgroundResource(R.drawable.backbround_white_color);
//
//
//
//                sharePref.setshareprefdatastring(SharePref.LANGUAGE,"bangla");
//                //  setBangla();
//
//                  lg = sharePref.getshareprefdatastring(SharePref.LANGUAGE);
//                Log.e("set",lg);
//                //Do what you want for create_button
//                break;
//            default:
//                break;
//        }
//
//    }
}

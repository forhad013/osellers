package invertemotech.com.osellers.activity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;

public class ForgetPassword extends AppCompatActivity {

    TextView topTxt,submit;

    EditText email;

    ImageButton backBtn;

    int success = 0 ;
    String message;

    JSONObject json;

    String emailString;





    boolean isInternetAvailable;
    ConnectionDetector cd;

    Typeface myTypeface, custom;

    SharePref sharePref;




    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);


        backBtn = (ImageButton) findViewById(R.id.backBtn);
        email = (EditText) findViewById(R.id.email);

        submit = (TextView) findViewById(R.id.submit);









      //  email.setTypeface(myTypeface);
//        submit.setTypeface(custom);
//        topTxt.setTypeface(custom);
//


        cd = new ConnectionDetector(getApplicationContext());

        isInternetAvailable = cd.isConnectingToInternet();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isInternetAvailable = cd.isConnectingToInternet();
                emailString =  email.getText().toString();
                progressSweetAlertDialog = new SweetAlertDialog(ForgetPassword.this, SweetAlertDialog.PROGRESS_TYPE);
                progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                progressSweetAlertDialog.setTitleText("Loading");
                progressSweetAlertDialog.setCancelable(false);

                submit.setEnabled(true);

                if(!emailString.isEmpty()) {
                    if(isInternetAvailable) {

                    }else{

                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }else{

                    doneDialog = new SweetAlertDialog(ForgetPassword.this, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setContentText("Give your email address");
                    doneDialog.show();
                }
            }
        });



    }

}

package invertemotech.com.osellers.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.adapter.LocationAdapter;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.retrofitmodel.Division;
import invertemotech.com.osellers.retrofitmodel.LocationResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationActivityPost extends AppCompatActivity {

    ListView list;
    TextView locstionTitle;

    Context context;

    ArrayList<Division> divisionsArrayList;
    ListView locations;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    Button done;
    TextView title1,title2,title3,title4;
    String selectedDivision="0",selectedDistrict="0",selectedArea="0";
    int divisionPosition,districtPosition,areaPosition;

    String districtName="";
    String divisionName="";
    String areaName="";

    boolean hasSetLocation = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
       overridePendingTransition(R.anim.slidein_from_bottom, R.anim.nothing);

        locations = (ListView) findViewById(R.id.list);

        done = (Button) findViewById(R.id.done);
        title1 = (TextView) findViewById(R.id.title1);
        title2 = (TextView) findViewById(R.id.title2);
        title3 = (TextView) findViewById(R.id.title3);
        title4 = (TextView) findViewById(R.id.title4);


        context = LocationActivityPost.this;

        progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        divisionPosition = getIntent().getIntExtra("division",200);
        districtPosition = getIntent().getIntExtra("district",200);
        areaPosition = getIntent().getIntExtra("area",200);
        hasSetLocation = getIntent().getBooleanExtra("setLocation",false);

        Log.e("areaPosition",areaPosition+"");
        Log.e("divisionPosition",divisionPosition+"");
        Log.e("districtPosition",districtPosition+"");

        title1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        title1.setTextColor(getResources().getColor(R.color.whitetext));


        title1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedDivision = "0";
                divisionPosition = 0;
                selectedDistrict = "0";
                districtPosition =0;
                selectedArea = "0";
                areaPosition =200;
                districtPosition =200;
                divisionPosition = 200;
                done.setEnabled(false);
                title1.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                title1.setTextColor(getResources().getColor(R.color.whitetext));


                title2.setVisibility(View.GONE);
                title3.setVisibility(View.GONE);
                title4.setVisibility(View.GONE);

                ArrayList<String> locationName = new ArrayList();

                for(int i=0;i<divisionsArrayList.size();i++){
                    locationName.add(divisionsArrayList.get(i).getDivName());
                }

                LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                locations.setAdapter(locationAdapter);
                LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);
            }
        });
      title2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                title2.setTextColor(getResources().getColor(R.color.whitetext));
                title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                title1.setTextColor(getResources().getColor(R.color.black));
                selectedDistrict = "0";
                districtPosition =0;
                selectedArea = "0";
                areaPosition =0;

                areaPosition =200;
                districtPosition =200;

                done.setEnabled(false);
                title3.setVisibility(View.GONE);
                title4.setVisibility(View.GONE);
                ArrayList<String>  locationName = new ArrayList();
                for(int i=0;i<divisionsArrayList.get(divisionPosition).getDistricts().size();i++){

                    locationName.add(divisionsArrayList.get(divisionPosition).getDistricts().get(i).getDisName());
                }

                LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                locations.setAdapter(locationAdapter);

                LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);
            }
        });
      title3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                done.setEnabled(false);
                areaPosition =200;

                title3.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                title3.setTextColor(getResources().getColor(R.color.whitetext));
                title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                title1.setTextColor(getResources().getColor(R.color.black));

                selectedArea = "0";
                areaPosition =0;
                title4.setVisibility(View.GONE);

                ArrayList<String>  locationName = new ArrayList();
                for(int i=0;i<divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().size();i++){

                    locationName.add(divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().get(i).getAreaName());
                }

                LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                locations.setAdapter(locationAdapter);
                LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);
            }
        });

        getLocations();

        locations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(selectedDivision.equals("0")){
                    done.setEnabled(false);
                    selectedDivision = divisionsArrayList.get(position).getDivId();
                    divisionName = divisionsArrayList.get(position).getDivName();
                    divisionPosition = position;
                    title2.setText(divisionsArrayList.get(position).getDivName());
                    title2.setVisibility(View.VISIBLE);
                    title2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    title2.setTextColor(getResources().getColor(R.color.whitetext));

                    title3.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title3.setTextColor(getResources().getColor(R.color.black));
                    title4.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title4.setTextColor(getResources().getColor(R.color.black));

                    title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title1.setTextColor(getResources().getColor(R.color.black));

                    ArrayList<String>  locationName = new ArrayList();
                    for(int i=0;i<divisionsArrayList.get(position).getDistricts().size();i++){

                        locationName.add(divisionsArrayList.get(position).getDistricts().get(i).getDisName());
                    }

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    locations.setAdapter(locationAdapter);
                    LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);
                }else if(!selectedDivision.equals("0") && selectedDistrict.equals("0")){

                    selectedDistrict = divisionsArrayList.get(divisionPosition).getDistricts().get(position).getDisId();
                    districtName = divisionsArrayList.get(divisionPosition).getDistricts().get(position).getDisName();
                    districtPosition = position;
                    title3.setText( divisionsArrayList.get(divisionPosition).getDistricts().get(position).getDisName());
                    title3.setVisibility(View.VISIBLE);
                    title3.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    title3.setTextColor(getResources().getColor(R.color.whitetext));


                    title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title1.setTextColor(getResources().getColor(R.color.black));
                    title2.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title2.setTextColor(getResources().getColor(R.color.black));
                    title4.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title4.setTextColor(getResources().getColor(R.color.black));

                    ArrayList<String>  locationName = new ArrayList();
                    for(int i=0;i< divisionsArrayList.get(divisionPosition).getDistricts().get(position).getAreas().size();i++){

                        locationName.add(divisionsArrayList.get(divisionPosition).getDistricts().get(position).getAreas().get(i).getAreaName());
                    }

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);

                    locations.setAdapter(locationAdapter);
                    LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);
                }else if(!selectedDivision.equals("0") &&!selectedDistrict.equals("0") && selectedArea.equals("0")){
                    done.setEnabled(true);
                    selectedArea = divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().get(position).getAreaId();
                    areaName = divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().get(position).getAreaName();
                    areaPosition = position;
                    title4.setText( divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().get(position).getAreaName());
                    title4.setVisibility(View.VISIBLE);

                    title4.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    title4.setTextColor(getResources().getColor(R.color.whitetext));


                    title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title1.setTextColor(getResources().getColor(R.color.black));
                    title2.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title2.setTextColor(getResources().getColor(R.color.black));
                    title3.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    title3.setTextColor(getResources().getColor(R.color.black));
                    ArrayList<String>  locationName = new ArrayList();

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    locations.setAdapter(locationAdapter);
                    LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);
                }
            }
        });


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hasSetLocation = true;
                Intent data = new Intent();
                if(divisionName.equals("")) {
                    data.putExtra("DivisionName","All Location");
                    data.putExtra("DivisionId","0");
                }else {
                    data.putExtra("DivisionName",divisionName);
                    data.putExtra("DivisionId",selectedDivision);
                }

                data.putExtra("DistrictName",districtName);

                data.putExtra("AreaName",areaName);
                data.putExtra("DistrictId",selectedDistrict);
                data.putExtra("setLocation",hasSetLocation);
                data.putExtra("AreaId",selectedArea);

                data.putExtra("division",divisionPosition);
                data.putExtra("district",districtPosition);
                data.putExtra("area",areaPosition);
                setResult(RESULT_OK,data);

                Log.e("areaPosition",areaPosition+"");
                Log.e("divisionPosition",divisionPosition+"");
                Log.e("districtPosition",districtPosition+"");

                finish();
                overridePendingTransition(R.anim.slide_up, R.anim.slideout_to_bottom);
            }
        });

    }

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(R.anim.slide_up, R.anim.slideout_to_bottom);

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {



        super.onDestroy();
    }


    public void getLocations(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);




        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<LocationResponse> call = apiService.getLocationList();
        call.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse>call, Response<LocationResponse> response) {


                //  topicsDataArrayList.clear();

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");
                divisionsArrayList = response.body().getData();

                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){



                    ArrayList<String> locationName = new ArrayList();

                    for(int i=0;i<divisionsArrayList.size();i++){
                        locationName.add(divisionsArrayList.get(i).getDivName());
                    }

                    LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(),locationName);
                    locations.setAdapter(locationAdapter);
                    LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);
                    progressSweetAlertDialog.dismiss();

                    if(hasSetLocation){
                       setPreviousData();
                    }


                }else {

                }




                progressSweetAlertDialog.dismiss();


            }

            @Override
            public void onFailure(Call<LocationResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());

                progressSweetAlertDialog.dismiss();
            }
        });


    }

    public void setPreviousData(){


        if(divisionPosition!=200) {
            selectedDivision = divisionsArrayList.get(divisionPosition).getDivName();

            selectedDistrict = "";

            selectedArea = "";

            title2.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            title2.setTextColor(getResources().getColor(R.color.whitetext));

            title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title1.setTextColor(getResources().getColor(R.color.black));
            title2.setText(divisionsArrayList.get(divisionPosition).getDivName());
            title2.setVisibility(View.VISIBLE);
            title3.setVisibility(View.GONE);
            title4.setVisibility(View.GONE);


            ArrayList<String> locationName = new ArrayList();
            for (int i = 0; i < divisionsArrayList.get(divisionPosition).getDistricts().size(); i++) {

                locationName.add(divisionsArrayList.get(divisionPosition).getDistricts().get(i).getDisName());
            }

            LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(), locationName);
            locations.setAdapter(locationAdapter);
            LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);

        }
        if(districtPosition!=200) {

            title3.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            title3.setTextColor(getResources().getColor(R.color.whitetext));
            title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title1.setTextColor(getResources().getColor(R.color.black));
            title2.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title2.setTextColor(getResources().getColor(R.color.black));
            selectedDistrict = divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getDisName();

            selectedArea = "";
            title3.setText(divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getDisName());
            title3.setVisibility(View.VISIBLE);
            title4.setVisibility(View.GONE);
            ArrayList<String> locationName = new ArrayList();
            for (int i = 0; i < divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().size(); i++) {

                locationName.add(divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().get(i).getAreaName());
            }


            LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(), locationName);
            locations.setAdapter(locationAdapter);

            LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);


        }
        if(areaPosition!=200) {


            title4.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            title4.setTextColor(getResources().getColor(R.color.whitetext));

            title1.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title1.setTextColor(getResources().getColor(R.color.black));

            title2.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title2.setTextColor(getResources().getColor(R.color.black));

            title3.setBackgroundColor(getResources().getColor(R.color.whitetext));
            title3.setTextColor(getResources().getColor(R.color.black));

            selectedArea = divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().get(areaPosition).getAreaName();;

            title4.setVisibility(View.VISIBLE);
            title4.setText(divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().get(areaPosition).getAreaName());

            ArrayList<String> locationName = new ArrayList();
//            for (int i = 0; i < divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().size(); i++) {
//
//                locationName.add(divisionsArrayList.get(divisionPosition).getDistricts().get(districtPosition).getAreas().get(i).getAreaName());
//            }


            LocationAdapter locationAdapter = new LocationAdapter(getApplicationContext(), locationName);
            locations.setAdapter(locationAdapter);
            LocationActivityPost.Utility.setListViewHeightBasedOnChildren(locations);
        }

    }


    public static class Utility {
        public static void setListViewHeightBasedOnChildren(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }
}

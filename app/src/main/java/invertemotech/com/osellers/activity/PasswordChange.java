package invertemotech.com.osellers.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.osellers.R;
import invertemotech.com.osellers.retrofitmodel.ApiClient;
import invertemotech.com.osellers.retrofitmodel.ApiInterafce;
import invertemotech.com.osellers.retrofitmodel.UpdateResponse;
import invertemotech.com.osellers.util.ConnectionDetector;
import invertemotech.com.osellers.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PasswordChange extends AppCompatActivity {



    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;


    Button update;

    String firstNameString, lastNameString, emailString, addressString, phoneString, userIdString, passwordString1, passwordString2;

    String oldPasswordString;



    boolean isInternetOn = false;

    SharePref sharePref;


    EditText password1, password2,oldPassword;
    Context context;

    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);

        context = PasswordChange.this;
        sharePref = new SharePref(context);
        oldPassword = (EditText) findViewById(R.id.oldPass);
        password1 = (EditText) findViewById(R.id.password1);
        password2 = (EditText) findViewById(R.id.password2);

        update = (Button) findViewById(R.id.update);
        cd = new ConnectionDetector(context);
        isInternetOn = cd.isConnectingToInternet();


        progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        ImageButton bckBtn = (ImageButton) findViewById(R.id.menu);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                oldPasswordString = oldPassword.getText().toString();
                passwordString1 = password1.getText().toString();
                passwordString2 = password2.getText().toString();
                cd = new ConnectionDetector(context);

                isInternetOn = cd.isConnectingToInternet();

                if (isInternetOn) {

                    if (passwordString1.equals(passwordString2)) {

                        changePassword();

                    } else {
                        doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        doneDialog.setTitleText("ERROR");
                        doneDialog.setContentText("Both password doesn't match");
                        doneDialog.show();
                        doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                doneDialog.dismiss();
                            }
                        });

                    }


                } else {
                    Toast.makeText(context, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


                }
            }
        });



    }


    @Override
    public void onResume() {


        oldPassword.setEnabled(true);
        password1.setEnabled(true);
        password2.setEnabled(true);
        super.onResume();
    }




    public void changePassword(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);


        Call<UpdateResponse> call = apiService.changePassword(Integer.parseInt(userIdString),oldPasswordString,passwordString1);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){


          


                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText(msg);
                    doneDialog.setConfirmText("Ok");
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();


                        }
                    });
                    doneDialog.show();




                }else {
                    progressSweetAlertDialog.dismiss();
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText("ERROR");
                    doneDialog.setContentText(msg);
                    doneDialog.show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }



//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }
}
